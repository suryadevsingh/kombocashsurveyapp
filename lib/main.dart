import 'package:animated_splash/animated_splash.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:splashscreen/splashscreen.dart';
import 'package:simple_splashscreen/simple_splashscreen.dart';
import 'Screens/Dbtoui.dart';
import 'Screens/auth/Signin.dart';
import 'Screens/survey/MockSurvey.dart';
import 'package:surveyapp/Screens/Instruction.dart';

import 'package:surveyapp/Screens/Dbtoui.dart';
void main() {
//  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
  return runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp]);
    return MaterialApp(
      title: "Kombo survey",
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        fontFamily: "Monteserrat",
        primaryColor: Colors.white,
      ),

      onGenerateRoute: (routeSettings) {
        // If you push the PassArguments route
        switch (routeSettings.name) {
          case '/mock_survey':
            return _buildRoute(routeSettings, new MockSurvey(routeSettings.arguments));;
            break;
            case '/instruction':
            return _buildRoute(routeSettings, new Instruction(routeSettings.arguments));;
            break;
            case '/surveyListing':
            return _buildRoute(routeSettings, new HomePage(routeSettings.arguments));;
            break;
        }
      },
      home: Container(
        height: 300,
        width: 300,
        child:
        Simple_splashscreen(
           gotoWidget: Signin(),
          splashscreenWidget: SplashScreen1(),
          timerInSeconds: 2,
        ),
//          AnimatedSplash(
//            home: Signin(),
//            imagePath: "assets/Images/launcher.png",
//            duration: 3,
//            type: AnimatedSplashType.BackgroundProcess,
//          ),
//        SplashScreen(
//          image: Image.asset(
//            "assets/Images/launcher.png",
//            fit: BoxFit.cover,
//          ),
//          seconds: 2,
//          photoSize: 100,
//          navigateAfterSeconds: Signin(),
//        ),
      ),
    );
  }

  MaterialPageRoute _buildRoute(RouteSettings settings, Widget builder) {
    return new MaterialPageRoute(
      settings: settings,
      builder: (ctx) => builder,
    );
  }
}

class SplashScreen1 extends StatefulWidget {
  @override
  _SplashScreen1State createState() => _SplashScreen1State();
}
class _SplashScreen1State extends State<SplashScreen1> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Image.asset("assets/Images/launcher.png",height: 200,width: 200,),
      ),
    );
  }
}