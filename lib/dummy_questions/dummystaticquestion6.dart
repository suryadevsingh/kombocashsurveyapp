

import 'package:flutter/material.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:surveyapp/Screens/Question3.dart';
import 'package:surveyapp/Screens/Surveycompleted.dart';

import 'dummyquestion7.dart';
class dummyquestion6 extends StatefulWidget {
  @override
  _dummyquestion6State createState() => _dummyquestion6State();
}

class _dummyquestion6State extends State<dummyquestion6> {

  List data ;
  int selectedRadioTile;
  setSelectedRadioTile(int val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  @override
  void initState() {
    super.initState();
//    selectedRadio = 0;
    selectedRadioTile = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.height *0.08,),
              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height *0.04,
                  width: MediaQuery.of(context).size.width *0.75,
                  child: FAProgressBar(
                    backgroundColor: Colors.grey[300],
                    progressColor: Color(0xFFb81f6f),
                    currentValue: 60,
//                  displayText: "%",
                  ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height *0.06,),
              Container(
                padding: EdgeInsets.all(27),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
//                  SizedBox(height: 27,),
                    Text("How long have you used product ?", style: TextStyle(fontWeight: FontWeight.w600,fontSize: 22),),
                  ],
                ),
//        child:    FutureBuilder(builder: (context, snapshot){
//              var showData =json.decode(snapshot.data.toString());
//              return ListView.builder(
//                itemBuilder: (BuildContext context , int index){
//                  return
//                    Text(showData[0].question_title);
////                    subtitle: Text(showData[index]["height"]),
//
//                },
//                itemCount: showData.length,
//              );
//            },
//              future: DefaultAssetBundle.of(context).loadString("load_json/survey.json"),
//            ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height *0.06,),
//          Expanded(
//            child: FutureBuilder(builder: (context, snapshot){
//              var showData =json.decode(snapshot.data.toString());
//              return ListView.builder(
//                itemBuilder: (BuildContext context , int index){
//                  return RadioListTile(
//                  title: Text(showData[index]["option"]["options"]),
//                  activeColor: Colors.blue,
//                    groupValue: selectedRadioTile,
//                    onChanged: null,
//                    value: index,
//
//                  );
//                },
//                itemCount: showData.length,
//              );
//            },
//              future: DefaultAssetBundle.of(context).loadString("load_json/person.json"),
//            ),
//          ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  RadioListTile(
                    value: 1,
                    title: Text("Less than a month"),
                    activeColor: Colors.blue,
                    groupValue:selectedRadioTile,
                    onChanged: (val){
                      print("Radio Tile pressed $val");
                      setSelectedRadioTile(val);
                    },
                  ),
                  RadioListTile(
                    value: 2,
                    title: Text("1-6 month"),
                    activeColor: Colors.blue,
                    groupValue:selectedRadioTile,
                    onChanged: (val){
                      print("Radio Tile pressed $val");
                      setSelectedRadioTile(val);
                    },
                  ),
                  RadioListTile(
                    value: 3,
                    title: Text("1-3 years"),
                    activeColor: Colors.blue,
                    groupValue:selectedRadioTile,
                    onChanged: (val){
                      print("Radio Tile pressed $val");
                      setSelectedRadioTile(val);
                    },
                  ),
RadioListTile(
  value: 4,
  title: Text("Never used"),
  activeColor: Colors.blue,
  groupValue:selectedRadioTile,
  onChanged: (val){
    print("Radio Tile pressed $val");
    setSelectedRadioTile(val);
  },
),

                ],
              ),

              SizedBox(height: MediaQuery.of(context).size.height *0.17,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
//                SizedBox(width: MediaQuery.of(context).size.width * 0.09,),
                  OutlineButton(onPressed: (){
                    Navigator.pop(context);
                  },
                    borderSide: BorderSide(color: Color(0xFFb81f6f)),
                    child: Text("Back",style: TextStyle(color: Color(0xFFb81f6f)),),),
//                SizedBox(width: MediaQuery.of(context).size.width* 0.40,),
                  MaterialButton(
                    onPressed: (){
                      Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: dummyquestion7()));
                    },
                    color: Color(0xFFb81f6f),
                    child: Text("Next",style: TextStyle(color: Colors.white),),
                  ),
//                SizedBox(width: MediaQuery.of(context).size.width * 0.08,),
                ],
              ),

            ],
          ),
        ),
      ),
    );
  }
}

