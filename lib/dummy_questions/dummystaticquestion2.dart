

import 'package:flutter/material.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:surveyapp/Screens/Question3.dart';

import 'dummystaticquestion3.dart';

class dummyquestion2 extends StatefulWidget {
  @override
  _dummyquestion2State createState() => _dummyquestion2State();
}

class _dummyquestion2State extends State<dummyquestion2> {

  List data ;
  int selectedRadioTile;
  setSelectedRadioTile(int val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  @override
  void initState() {
    super.initState();
//    selectedRadio = 0;
    selectedRadioTile = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.height *0.08,),
              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height *0.04,
                  width: MediaQuery.of(context).size.width *0.75,
                  child: FAProgressBar(
                    backgroundColor: Colors.grey[300],
                    progressColor: Color(0xFFb81f6f),
                    currentValue: 20,
//                  displayText: "%",
                  ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height *0.06,),
              Container(
                padding: EdgeInsets.all(27),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
//                  SizedBox(height: 27,),
                    Text("How would you rate your experience with our  product ?", style: TextStyle(fontWeight: FontWeight.w600,fontSize: 22),),
//                    SizedBox(height: 5,),
//                    Text("", style: TextStyle(fontWeight: FontWeight.w600,fontSize: 22),),
//                    SizedBox(height: 5,),
//                    Text("", style: TextStyle(fontWeight: FontWeight.w600,fontSize: 22),),
                  ],
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height *0.06,),
//          Expanded(
//            child: FutureBuilder(builder: (context, snapshot){
//              var showData =json.decode(snapshot.data.toString());
//              return ListView.builder(
//                itemBuilder: (BuildContext context , int index){
//                  return RadioListTile(
//                  title: Text(showData[index]["option"]["options"]),
//                  activeColor: Colors.blue,
//                    groupValue: selectedRadioTile,
//                    onChanged: null,
//                    value: index,
//
//                  );
//                },
//                itemCount: showData.length,
//              );
//            },
//              future: DefaultAssetBundle.of(context).loadString("load_json/person.json"),
//            ),
//          ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  RadioListTile(
                    value: 1,
                    title: Text("Very satisfied"),
                    activeColor: Colors.blue,
                    groupValue:selectedRadioTile,
                    onChanged: (val){
                      print("Radio Tile pressed $val");
                      setSelectedRadioTile(val);
                    },
                  ),
                  RadioListTile(
                    value: 2,
                    title: Text("Satisfied"),
                    activeColor: Colors.blue,
                    groupValue:selectedRadioTile,
                    onChanged: (val){
                      print("Radio Tile pressed $val");
                      setSelectedRadioTile(val);
                    },
                  ),
                  RadioListTile(
                    value: 3,
                    title: Text("Dissatisfied"),
                    activeColor: Colors.blue,
                    groupValue:selectedRadioTile,
                    onChanged: (val){
                      print("Radio Tile pressed $val");
                      setSelectedRadioTile(val);
                    },
                  ),
                  RadioListTile(
                    value: 4,
                    title: Text("Very dissatisfied"),
                    activeColor: Colors.blue,
                    groupValue:selectedRadioTile,
                    onChanged: (val){
                      print("Radio Tile pressed $val");
                      setSelectedRadioTile(val);
                    },
                  ),
                ],
              ),

              SizedBox(height: MediaQuery.of(context).size.height *0.17,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
//                SizedBox(width: MediaQuery.of(context).size.width * 0.09,),
                  OutlineButton(onPressed: (){
                    Navigator.pop(context);
                  },
                    borderSide: BorderSide(color: Color(0xFFb81f6f)),
                    child: Text("Back",style: TextStyle(color: Color(0xFFb81f6f)),),),
//                SizedBox(width: MediaQuery.of(context).size.width* 0.40,),
                  MaterialButton(
                    onPressed: (){
                      Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: dummyquestion3()));
                    },
                    color: Color(0xFFb81f6f),
                    child: Text("Next",style: TextStyle(color: Colors.white),),
                  ),
//                SizedBox(width: MediaQuery.of(context).size.width * 0.08,),
                ],
              ),

            ],
          ),
        ),
      ),
    );
  }
}

