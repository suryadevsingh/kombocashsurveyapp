import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:surveyapp/backend/db_provider.dart';

class AppBarTemplate extends StatelessWidget with PreferredSizeWidget{
  String title;
  String subTitle;
  bool hasBackButton;

  AppBarTemplate(String title, String subtitle, bool hesBackButton) {
    this.title = title;
    this.subTitle = subtitle;
    this.hasBackButton = hasBackButton;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return new AppBar(
      automaticallyImplyLeading: false,
        title: new Text(title + "(" + subTitle + ")"),
//        leading: hasBackButton
//            ??
//            IconButton(
//              icon: Icon(CupertinoIcons.back, color: Color(0xFF8a8989),),
//              tooltip: 'Back',
//              onPressed: () {
//                Navigator.pop(context);
//              },
//            ),
    actions: <Widget>[
      Container(
        padding: EdgeInsets.only(right: 10.0),
        child: IconButton(
          icon: Icon(CupertinoIcons.delete, color: Color(0xFF8a8989),),
          tooltip: "Delete Responses",
          onPressed: () async {
            await DBProvider.db.deleteAllPreviousOptionsStored();
          },
        ),
      )
    ],);
  }

  @override
  // TODO: implement preferredSize
  Size get preferredSize =>  Size.fromHeight(kToolbarHeight);
}
