import 'package:auto_size_text/auto_size_text.dart';
import 'package:better_uuid/uuid.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:surveyapp/Screens/survey/pojo/MockQuestionPojo.dart';
import 'package:surveyapp/Screens/survey/pojo/OptionDataStorage.dart';
import 'package:surveyapp/backend/db_provider.dart';
import 'package:surveyapp/eventBus/RedirectToPage.dart';
import 'package:surveyapp/utils/Utility.dart';
import 'package:surveyapp/eventBus/globalEventBus.dart';

import '../Surveycompleted.dart';
import 'pojo/MockSurveyPojo.dart';

class MockQuestionPage extends StatefulWidget {

//  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  MockQuestionPojo mockQuestionPojo;
  Function(int) callback;
  MockSurveyPojo mockSurveyPojo;

  MockQuestionPage(MockQuestionPojo mockQuestionPojo, Function(int) callback, MockSurveyPojo mockSurveyPojo) {
    this.mockQuestionPojo = mockQuestionPojo;
    this.callback = callback;
    this.mockSurveyPojo = mockSurveyPojo;
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _MockQuestionPage(mockQuestionPojo, callback,mockSurveyPojo);
  }
}

class _MockQuestionPage extends State {

  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  MockQuestionPojo mockQuestionPojo;
  MockSurveyPojo mockSurveyPojo;
  Function(int) callback;
  int _groupValue = -1;

  _MockQuestionPage(MockQuestionPojo mockQuestionPojo, Function(int) callback,MockSurveyPojo mockSurveyPojo) {
    this.mockQuestionPojo = mockQuestionPojo;
    this.callback = callback;
    this.mockSurveyPojo = mockSurveyPojo;
  }

  Future<int> checkIfCheckboxIsChecked(String optionId) async {

    var previousResponse = await DBProvider.db.findIfOptionisCheckedEarlier(
        mockQuestionPojo.questionId,
        await Utility.getCurrentUserIdGivingSurvey(),
        optionId);
    print('previousResponse for option');
    print(previousResponse);
    if (previousResponse.length == 0) {
      return Future(() => 0);
    }
    return Future(() => 1);
  }

  Future<int> doStuffCallback() async {

    var data = await DBProvider.db.fetchQuestionResponse(
        mockQuestionPojo.questionId, await Utility.getCurrentUserIdGivingSurvey());

    if (data.length != 0) {
      var allOptions =
          await DBProvider.db.fetchQuestionOptions(mockQuestionPojo.questionId);
      var flag = -1;
//      var flag = 1;
      for (var j = 0; j < data.length; j++) {
        for (var i = 0; i < allOptions.length; i++) {
          if (data[j]["option_id"] == allOptions[i]["option_id"]) {
            flag = i;
            print('Matched');
            break;
          }
        }
      }
      print("Returning Future");
      return Future(() => flag);
    } else {
      return Future(() => -1);
    }
  }

  callCallaback(int page) {
//    callback(page);
    eventBus.fire(RedirectToPage(page));
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return FutureBuilder(
        future: doStuffCallback(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          print("Data in snapshot");
          print(snapshot);
          if (!snapshot.hasData) {
            return Center(
              child: CircularProgressIndicator(),
            );
          } else {
            print(snapshot.data);
            print("Data returned from the snapshot");
            _groupValue = snapshot.data;
            int currentProgress = (((mockQuestionPojo.index  +1)/mockQuestionPojo.totalQuestions )*100).round();
            return new SingleChildScrollView(
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.03,
                  ),
                  Center(
                    child: Container(
                      height: MediaQuery.of(context).size.height *0.04,
                      width: MediaQuery.of(context).size.width *0.75,
                      child: FAProgressBar(
                        backgroundColor: Colors.grey[300],
                        progressColor: Color(0xFFb81f6f),
                        currentValue: currentProgress,
//                  displayText: "%",
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.all(16),
                    child: Align(
                      alignment: Alignment.centerLeft,
                      child: AutoSizeText(
                        mockQuestionPojo.question.toString(),
                        style:
                            TextStyle(fontSize: 20, fontWeight: FontWeight.bold,),
                        textAlign: TextAlign.left,
                        maxFontSize: 20,
                        minFontSize: 6,
                      ),
                    ),
                  ),
                  fetchQuestionOptions(mockQuestionPojo.questionId),
                ],
              ),
            );
          }
        });
  }

  fetchQuestionOptions(String questionId) {
    return FutureBuilder(
      future: DBProvider.db.fetchQuestionOptions(questionId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return new Column(
            children: <Widget>[
              ListView.builder(
                shrinkWrap: true,
                itemCount: snapshot.data.length,
                physics: new NeverScrollableScrollPhysics(),
                itemBuilder: (BuildContext context, int index) {
                  String value = snapshot.data[index]["choices"].toString();
                  if (mockQuestionPojo.questionType
                      .toString()
                      .contains("Select One")) {
//                      .contains("One")) {
                    return _myRadioButton(
                      title: value,
                      value: index,
                      onChanged: (newValue) {

                        if (_groupValue == -1 )
                        {

                            storeInDatabase(snapshot.data[index]["option_id"]);

                        print("option 1");
                      print(snapshot.data[index]["option_id"]);


                        }
                        else
                          {

                            updateInDatabase(snapshot.data[index]["option_id"]);
                            print("option 2");
                      print(snapshot.data[index]["option_id"]);
                          }
                        setState(() {


                        });
                        return ;
                      },
                    );
                  } else {
                    String optionId =
                        snapshot.data[index]["option_id"].toString();
                    return FutureBuilder(
                      future: checkIfCheckboxIsChecked(optionId),
                      builder: (BuildContext context, AsyncSnapshot snapshot) {
                        if (snapshot.hasData) {
                          bool b = snapshot.data == 1;
                          return _myCheckBox(
                            title: value,
                            value: b,
                            onChanged: (newValue) {
//                              Utility.showToastMessages("message");
                              storeOrUpdateDatabase(optionId);
                              setState(() {});
                              return;
                            },
                          );
                        } else {
                          return _myCheckBox(
                            title: value,
                            value: false,
                            onChanged: (newValue) {
                              Scaffold.of(context).showSnackBar(SnackBar(
                                content: Text('Something went wrong'),
                                backgroundColor: Colors.red,
                                duration: Duration(seconds: 1),
                              ));
//                              Utility.showToastMessages("Something went wrong");
                              return;
                            },
                          );
                        }
                      },
                    );
                  }
                },
              ),
              Container(
                padding: EdgeInsets.all(32.0),
                child: new Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    mockQuestionPojo.index != 0
                        ? OutlineButton(
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                            borderSide: BorderSide(color: Color(0xFFb81f6f)),
                            onPressed: () {
                              callCallaback(mockQuestionPojo.index - 1);
                            },
                            child: const Text('Back',
                                style: TextStyle(
                                    fontSize: 16, color: Color(0xFFb81f6f))),
                          )
                        : new SizedBox.shrink(),
                    RaisedButton(
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                      color: Color(0xFFb81f6f),
                      onPressed: () async {
                        //Store in Database
                        if(mockQuestionPojo.questionType.contains("One")){
                          if (_groupValue == -1) {
                            Scaffold.of(context).showSnackBar(SnackBar(
                              content: Text('Please select any option'),
                              backgroundColor: Colors.red,
                              duration: Duration(seconds: 1),
                            ));

//                            Utility.showToastMessages("Please select any option");
                          } else {

//                            await storeInDatabase(
//                                snapshot.data[_groupValue]["option_id"]);

                            if (fetchQuestionText() == "Next") {
                              callCallaback(mockQuestionPojo.index + 1);
                            } else {
//                              Utility.showToastMessages(
//                                  "Survey Finished, Thanks!");
                              Navigator.of(context).push(PageTransition(
                                  type: PageTransitionType.slideZoomLeft,
                                  child: Surveycompleted(mockSurveyPojo)));

//                            Surveycompleted();
                            }
                          }
                        }else{
                          checkIfResponseCaptured();
                        }

                      },
                      child: Text(fetchQuestionText(),
                          style: TextStyle(fontSize: 16, color: Colors.white)),
                    ),
                  ],
                ),
              ),
            ],
          );
        }
      },
    );
  }

  Widget _myRadioButton({String title, int value, Function onChanged}) {
    return RadioListTile(
//      key: _scaffoldKey,
      value: value,
      groupValue: _groupValue,
      onChanged: onChanged,
      title: Text(title),
    );
  }

  Widget _myCheckBox({String title, bool value, Function onChanged}) {
    return CheckboxListTile(
//       key: _scaffoldKey,
      controlAffinity: ListTileControlAffinity.leading,
      onChanged: onChanged,
      title: Text(title),
      value: value,
    );
  }

  void storeOrUpdateDatabase(String optionId) async {
    OptionDataStorage optionDataStorage = new OptionDataStorage();
    optionDataStorage.optionId = optionId;
    optionDataStorage.questionId = mockQuestionPojo.questionId;
    optionDataStorage.surveyId = mockSurveyPojo.surveyId;
    optionDataStorage.userId = await Utility.getCurrentUserIdGivingSurvey();
    Map<String, dynamic> questionResponse = {
      DBProvider.question_id: optionDataStorage.questionId,
      DBProvider.user_id: optionDataStorage.userId,
      DBProvider.option_id: optionDataStorage.optionId,
      DBProvider.surveyId: optionDataStorage.surveyId,
      DBProvider.createdAt: Uuid.v1().time.toString(),
    };

    var previousResponse = await DBProvider.db.findIfOptionisCheckedEarlier(
        optionDataStorage.questionId, optionDataStorage.userId, optionId);
    print('previousResponse');
    print(previousResponse);
    if (previousResponse.length == 0) {
      // check
      DBProvider.db.addQuestionResponse(questionResponse);
    } else {
      // Uncheck
      DBProvider.db.removeQuestionResponse(
          optionDataStorage.questionId, optionDataStorage.userId, optionId);
    }
  }

  void storeInDatabase(String optionId) async {
    OptionDataStorage optionDataStorage = new OptionDataStorage();
    optionDataStorage.optionId = optionId;
    optionDataStorage.questionId = mockQuestionPojo.questionId;
    optionDataStorage.userId = await Utility.getCurrentUserIdGivingSurvey();
    optionDataStorage.surveyId= mockSurveyPojo.surveyId;
    Map<String, dynamic> questionResponse = {
      DBProvider.question_id: optionDataStorage.questionId,
      DBProvider.user_id: optionDataStorage.userId,
      DBProvider.option_id: optionDataStorage.optionId,
      DBProvider.surveyId: optionDataStorage.surveyId,
      DBProvider.createdAt: Uuid.v1().time.toString(),
    };
    await DBProvider.db.addQuestionResponse(questionResponse);
  }

  void updateInDatabase(String optionId) async {
    OptionDataStorage optionDataStorage = new OptionDataStorage();
    optionDataStorage.optionId = optionId;
    optionDataStorage.questionId = mockQuestionPojo.questionId;
    optionDataStorage.surveyId = mockSurveyPojo.surveyId;
    optionDataStorage.userId = await Utility.getCurrentUserIdGivingSurvey();
    Map<String, dynamic> questionResponse = {
      DBProvider.question_id: optionDataStorage.questionId,
      DBProvider.user_id: optionDataStorage.userId,
      DBProvider.option_id: optionDataStorage.optionId,
      DBProvider.surveyId: optionDataStorage.surveyId,
      DBProvider.createdAt: Uuid.v1().time.toString(),
    };
    await DBProvider.db.updateQuestionResponse(questionResponse);
  }

  String fetchQuestionText() {
    if (mockQuestionPojo.index != mockQuestionPojo.totalQuestions - 1) {
      return "Next";
    } else {
      return "Finish";
    }
  }

  void checkIfResponseCaptured() async{
    var data = await DBProvider.db.fetchQuestionResponse(
        mockQuestionPojo.questionId, await Utility.getCurrentUserIdGivingSurvey());
    if(data.length == 0){
      Scaffold.of(context).showSnackBar(SnackBar(
        content: Text('Please select any option'),
        duration: Duration(seconds: 1),
        backgroundColor: Colors.red,
      ));
//      Utility.showToastMessages("Please select any option");

    }else{
      if (fetchQuestionText() == "Next") {
        callCallaback(mockQuestionPojo.index + 1);
      } else {

//        Utility.showToastMessages(
//            "Survey Finished, Thanks!");

        Navigator.of(context).push(PageTransition(
            type: PageTransitionType.slideZoomLeft,
            child: Surveycompleted(mockSurveyPojo)));
      }
    }
  }
}
