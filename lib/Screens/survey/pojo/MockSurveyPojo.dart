class MockSurveyPojo {
  String _surveyId;
  String _surveyName;
  String _totalQuestions;
  String _totalSurvey;
  String _surveyType;


  String get totalSurvey => _totalSurvey;

  set totalSurvey(String value) {
    _totalSurvey = value;
  }

  String get surveyType => _surveyType;

  set surveyType(String value) {
    _surveyType = value;
  }

  String get surveyName => _surveyName;

  set surveyName(String value) {
    _surveyName = value;
  }

  String get surveyId => _surveyId;

  set surveyId(String value) {
    _surveyId = value;
  }

  String get totalQuestions => _totalQuestions;

  set totalQuestions(String value) {
    _totalQuestions = value;
  }
}
