class OptionDataStorage{
  String _questionId;
  String _optionId;
  String _userId;
  String _surveyId;


  String get surveyId => _surveyId;

  set surveyId(String value) {
    _surveyId = value;
  }

  String get questionId => _questionId;

  set questionId(String value) {
    _questionId = value;
  }

  String get optionId => _optionId;

  String get userId => _userId;

  set userId(String value) {
    _userId = value;
  }

  set optionId(String value) {
    _optionId = value;
  }


}