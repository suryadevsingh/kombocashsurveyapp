class MockQuestionPojo{
    String _questionId;
    String _question;
    String _questionType;

    String get questionType => _questionType;

    set questionType(String value) {
      _questionType = value;
    }

    int _index;
    int _totalQuestions;
    int get index => _index;

    set index(int value) {
      _index = value;
    }

    String get questionId => _questionId;

    set questionId(String value) {
      _questionId = value;
    }

    String get question => _question;

    set question(String value) {
      _question = value;
    }

    int get totalQuestions => _totalQuestions;

    set totalQuestions(int value) {
      _totalQuestions = value;
    }


}