import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:surveyapp/Screens/survey/pojo/MockQuestionPojo.dart';
import 'package:surveyapp/Screens/survey/templates/appBar.dart';
import 'package:surveyapp/backend/db_provider.dart';
import 'package:surveyapp/eventBus/RedirectToPage.dart';
import 'package:surveyapp/eventBus/globalEventBus.dart';
import 'package:surveyapp/utils/Utility.dart';
import '../Dashboard.dart';
import 'MockQuestionPage.dart';
import 'pojo/MockSurveyPojo.dart';

class MockSurvey extends StatefulWidget {
  MockSurveyPojo mockSurveyPojo;
  MockSurvey(MockSurveyPojo mockSurveyPojo) {
    this.mockSurveyPojo = mockSurveyPojo;
  }
  @override
  State<StatefulWidget> createState() {

    // TODO: implement createState
    return new _MockSurvey(mockSurveyPojo);
  }
}

class _MockSurvey extends State {
  final GlobalKey<ScaffoldState> _scaffoldKey1 = new GlobalKey<ScaffoldState>();
  MockSurveyPojo mockSurveyPojo;
//  static PageController pageViewController;
   PageController pageViewController;

  MockQuestionPojo mockQuestionPojo ;

  Future<bool>alert(){
  if (pageViewController.initialPage == PageController(initialPage: 0))  {

        return _onWillPop();

  }
  else{
//    return _onWillPop() ;
  }
}

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to leave the survey?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () {
              Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Dashboard()));
            },
            child: new Text('Yes'),
          ),
        ],
      ),
    ) ??
        false;
  }

  @override
  void initState() {
    pageViewController = new PageController(initialPage: 0, keepPage: false);
  configureEventBus();
  }

  _MockSurvey(MockSurveyPojo mockSurveyPojo) {
    this.mockSurveyPojo = mockSurveyPojo;
  }

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return WillPopScope(
      onWillPop: alert,
      child: new Scaffold(
        key: _scaffoldKey1,
        appBar: AppBarTemplate(mockSurveyPojo.surveyName.toString(),
            mockSurveyPojo.totalQuestions.toString(), true,),
        body: new Container(
          child: showAllQuestionsWhereSurveyId(mockSurveyPojo.surveyId),
        ),
      ),
    );
  }

  showAllQuestionsWhereSurveyId(String surveyId) {
    return FutureBuilder(
      future: DBProvider.db.fetchSurveyQuestionsBasedOnId(surveyId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        else {
          return PageView.builder(

            itemCount: snapshot.data.length,
              controller: pageViewController,
              physics: new NeverScrollableScrollPhysics(),
              itemBuilder: (BuildContext context, int index) {
              MockQuestionPojo mockQuestionPojo = new MockQuestionPojo();
              mockQuestionPojo.question = snapshot.data[index]["questionName"].toString();
              mockQuestionPojo.questionId = snapshot.data[index]["questionId"].toString();
              mockQuestionPojo.questionType= snapshot.data[index]["question_type"].toString();
              mockQuestionPojo.index = index;
              mockQuestionPojo.totalQuestions = snapshot.data.length;
              print(snapshot.data[index]["question_type"].toString());
              print('mockSurveyPojo');
              print(mockSurveyPojo.surveyId);
              print(mockSurveyPojo.surveyName);

              return MockQuestionPage(mockQuestionPojo,changePageTo,mockSurveyPojo);
              },
          );
        }
      },
    );
  }

//  static changePageTo(int pageNo){
//    pageViewController.jumpToPage(pageNo);
//  }

   changePageTo(int pageNo){
    pageViewController.jumpToPage(pageNo);
  }

  void configureEventBus() {

    eventBus.on<RedirectToPage>().listen((event) {
//      Utility.showToastMessages("redirect");
      pageViewController.jumpToPage(event.page);
    });
  }

}
