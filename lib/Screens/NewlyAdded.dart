import 'package:flutter/material.dart';

class NewlyAdded extends StatefulWidget {
  @override
  _NewlyAddedState createState() => _NewlyAddedState();
}

class _NewlyAddedState extends State<NewlyAdded> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(title: Text("Newly added (4)",style: TextStyle(color: Colors.red,fontSize: 30),
        ),
          titleSpacing: 0,
          leading: IconButton(icon: Icon(Icons.arrow_back_ios,color: Colors.grey,), onPressed: (){
          Navigator.pop(context);
          },),
          elevation: 0,
        ),
        body: Container(
          color: Colors.white,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.height *0.03,),
              Card(
                elevation: 2,
                child: Container(
                  color: Color(0xFFf3f3f3),
                  width: MediaQuery.of(context).size.width *0.90,
                  height: MediaQuery.of(context).size.height *0.12,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Support Statisfaction",style: TextStyle(color: Colors.deepPurple,fontSize: 26,fontWeight: FontWeight.bold),),
                        Text("Created on: Feb 3,2020 | Responses: 0 / 100",style: TextStyle(color: Colors.deepPurple,fontSize: 14,fontWeight: FontWeight.w500),),
                      ],
                    ),
                  ),
                ),
              ),

              SizedBox(height: MediaQuery.of(context).size.height *0.0355,),
              Card(
                elevation: 2,
                child: Container(
                  color: Color(0xFFf3f3f3),
                  width: MediaQuery.of(context).size.width *0.90,
                  height: MediaQuery.of(context).size.height *0.12,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Conflicts of Crimes",style: TextStyle(color: Colors.deepPurple,fontSize: 26,fontWeight: FontWeight.bold),),
                        Text("Created on: Feb 2,2020 | Responses: 1 / 100",style: TextStyle(color: Colors.deepPurple,fontSize: 14,fontWeight: FontWeight.w500),),
                      ],
                    ),
                  ),
                ),
              ),

              SizedBox(height: MediaQuery.of(context).size.height *0.0355,),
              Card(
                elevation: 2,
                child: Container(
                  color: Color(0xFFf3f3f3),
                  width: MediaQuery.of(context).size.width *0.90,
                  height: MediaQuery.of(context).size.height *0.12,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Gender Equality",style: TextStyle(color: Colors.deepPurple,fontSize: 26,fontWeight: FontWeight.bold),),
                        Text("Created on: Feb 4,2020 | Responses: 3 / 100",style: TextStyle(color: Colors.deepPurple,fontSize: 14,fontWeight: FontWeight.w500),),
                      ],
                    ),
                  ),
                ),
              ),

              SizedBox(height: MediaQuery.of(context).size.height *0.0355,),
              Card(
                elevation: 2,
                child: Container(
                  color: Color(0xFFf3f3f3),
                  width: MediaQuery.of(context).size.width *0.90,
                  height: MediaQuery.of(context).size.height *0.12,
                  child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text("Cleanliness in the City",style: TextStyle(color: Colors.deepPurple,fontSize: 26,fontWeight: FontWeight.bold),),
                        Text("Created on: Feb 4,2020 | Responses: 1 / 100",style: TextStyle(color: Colors.deepPurple,fontSize: 14,fontWeight: FontWeight.w500),),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),

      ),
    );
  }
}
