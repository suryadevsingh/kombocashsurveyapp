import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:surveyapp/Screens/survey/pojo/MockSurveyPojo.dart';
import 'package:surveyapp/backend/db_provider.dart';
import 'package:surveyapp/backend/local_json.dart';

import 'Instruction.dart';

class HomePage extends StatefulWidget {
  MockSurveyPojo mockSurveyPojo;

  HomePage(MockSurveyPojo mockSurveyPojo){
    this.mockSurveyPojo = mockSurveyPojo;
  }

  @override
  _HomePageState createState() => _HomePageState(mockSurveyPojo);
}

class _HomePageState extends State<HomePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey1 = new GlobalKey<ScaffoldState>();
  var isLoading = false;
  MockSurveyPojo mockSurveyPojo;

  _HomePageState(MockSurveyPojo mockSurveyPojo){
    this.mockSurveyPojo = mockSurveyPojo;
  }

  _checkInternetConnectivity() async {
    var result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.none) {



      _scaffoldKey1.currentState.showSnackBar(SnackBar(
        content: Text("Please check your internet connectivity"),
        duration: Duration(seconds: 1),

        backgroundColor: Colors.red,
      ));

    } else {

      _scaffoldKey1.currentState.showSnackBar(SnackBar(
        content: Text("Hold a second "),
        duration: Duration(seconds: 1),
        backgroundColor: Colors.green,
      ));
      _loadFromApi();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey1,
      appBar: AppBar(
        title: Text(
          mockSurveyPojo.surveyType == "newly"? 'Newly Added ':"Available Survey " + mockSurveyPojo.totalQuestions,
          style: TextStyle(color: Color(0xFF002632), fontSize: 18),
        ),
//        centerTitle: true,
        actions: <Widget>[
          Container(
            padding: EdgeInsets.only(right: 10.0),
            child: IconButton(
              icon:
              Icon(CupertinoIcons.refresh_circled, color: Color(0xFF8a8989),),
//              onPressed: () async {
//                await _loadFromApi();
//              },
            onPressed: (){
              _checkInternetConnectivity();
            },
            ),
          ),
          Container(
            padding: EdgeInsets.only(right: 10.0),
            child: IconButton(
              icon: Icon(CupertinoIcons.delete, color: Color(0xFF8a8989),),
              onPressed: () async {
                await _deleteData();
              },
            ),
          ),
        ],
      ),
      body:
//      choicessurvey(),

      mockSurveyPojo.surveyType == "newly" ? shownewSurveys() : showSurveys() );

//        if( mockSurveyPojo.surveyType == "newly") {
//       return shownewSurveys();
//      } else {
//    return showSurveys();
//      }
      
//          showSurveys(),
    
//      _buildEmployeeListView(),
//    );

  }

  _loadFromApi() async {
    setState(() {
      isLoading = true;
    });

    var apiProvider = SurveyLocalJson();
    await apiProvider.loadAsset(apiProvider.Mydata);
//    await apiProvider.loadAsset(context);

    // wait for 2 seconds to simulate loading of data
    await Future.delayed(const Duration(seconds: 1));

    setState(() {
      isLoading = false;
    });
  }

  _deleteData() async {
    setState(() {
      isLoading = true;
    });

    await DBProvider.db.deleteOnlyAllSurvey();
//    await DBProvider.db.getAllSurvey();

    // wait for 1 second to simulate loading of data
    await Future.delayed(const Duration(seconds: 1));

    setState(() {
      isLoading = false;
    });

    print('All employees deleted');
  }

  ///function for questions

  showSurveys() {
    return FutureBuilder(
      future: DBProvider.db.getAllSurvey(),
//      future: DBProvider.db.getAllSurvey(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.white,
              height: MediaQuery.of(context).size.height * 0,
            ),
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              String subtitle = "";
//              if(snapshot.data[index][""]){
//
//              }
              if (snapshot.data[index]["total_questions"] != null) {
                subtitle = "Total Questions: " +
                    snapshot.data[index]["total_questions"];
              }
              return
//                ListTile(
//                leading: Text(
//                  "${index + 1}",
//                  style: TextStyle(fontSize: 20.0),
//                ),
//                title: Text(
//                  snapshot.data[index]["survey_title"].toString(),
//                ),
//                subtitle: Text(subtitle),
//                onTap: () {
//                  String surveyId = snapshot.data[index]["survey_id"]
//                      .toString();
//                  MockSurveyPojo mockSurveyPojo = new MockSurveyPojo();
//                  mockSurveyPojo.surveyId = surveyId;
//                  mockSurveyPojo.surveyName =
//                      snapshot.data[index]["survey_title"].toString();
//                  mockSurveyPojo.totalQuestions =
//                  snapshot.data[index]["total_questions"];
//                  Navigator.pushNamed(
//                      context, "/mock_survey", arguments: mockSurveyPojo);
//                },
//              );
                  Container(
                margin: EdgeInsets.all(5),
                width: MediaQuery.of(context).size.width * 0.90,
                height: MediaQuery.of(context).size.height * 0.12,
                child: GestureDetector(
//                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    String surveyId =
                        snapshot.data[index]["survey_id"].toString();
                    MockSurveyPojo mockSurveyPojo = new MockSurveyPojo();
                    mockSurveyPojo.surveyId = surveyId;
                    mockSurveyPojo.surveyName =
                        snapshot.data[index]["survey_title"].toString();
                    mockSurveyPojo.totalQuestions =
                        snapshot.data[index]["total_questions"];
//                    Navigator.pushNamed(context, "/mock_survey",
//                        arguments: mockSurveyPojo);

//                    Navigator.of(context).push(route)

                    Navigator.of(context).push(PageTransition(
                        type: PageTransitionType.slideZoomLeft,
                        child: Instruction(mockSurveyPojo))
                    );


//                    Navigator.pushNamed(context, "/instruction",
//                        arguments: mockSurveyPojo);
                  },
                  child: Card(
//                    padding: EdgeInsets.all(20),
//                  margin: EdgeInsets.all(10),
                    color: Color(0xFFf3f3f3),
                    elevation: 2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
          Row(children: <Widget>[
           SizedBox(width: 30,),
            ///

            Text(
              snapshot.data[index]["survey_title"].toString(),
              style: TextStyle(
                  color: Color(0xFF525151),
                  fontSize: 18,
                  fontWeight: FontWeight.normal),
            ),

            ///
                    ],),

//                        Text(
//                          snapshot.data[index]["survey_title"].toString(),
//                          style: TextStyle(
//                              color: Color(0xFF525151),
//                              fontSize: 18,
//                              fontWeight: FontWeight.normal),
//                        ),
                      Row(children: <Widget>[
                        SizedBox(width: 30,),
                        Text(
                          subtitle,
                          style: TextStyle(
                              color: Color(0xFF525151),
                              fontSize: 14,
                              fontWeight: FontWeight.w500),
                        ),
                      ],),

                      ],
                    ),
                  ),
                ),
              );
            },
          );
        }
      },
    );
  }
  
  
  shownewSurveys() {
    return FutureBuilder(
      future: DBProvider.db.fetchAllNewlyAddedSurvey(),
//      future: DBProvider.db.getAllSurvey(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
          return ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.white,
              height: MediaQuery.of(context).size.height * 0,
            ),
            itemCount: snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              String subtitle = "";
//              if(snapshot.data[index][""]){
//
//              }
              if (snapshot.data[index]["total_questions"] != null) {
                subtitle = "Total Questions: " +
                    snapshot.data[index]["total_questions"];
              }
              return
//                ListTile(
//                leading: Text(
//                  "${index + 1}",
//                  style: TextStyle(fontSize: 20.0),
//                ),
//                title: Text(
//                  snapshot.data[index]["survey_title"].toString(),
//                ),
//                subtitle: Text(subtitle),
//                onTap: () {
//                  String surveyId = snapshot.data[index]["survey_id"]
//                      .toString();
//                  MockSurveyPojo mockSurveyPojo = new MockSurveyPojo();
//                  mockSurveyPojo.surveyId = surveyId;
//                  mockSurveyPojo.surveyName =
//                      snapshot.data[index]["survey_title"].toString();
//                  mockSurveyPojo.totalQuestions =
//                  snapshot.data[index]["total_questions"];
//                  Navigator.pushNamed(
//                      context, "/mock_survey", arguments: mockSurveyPojo);
//                },
//              );
                  Container(
                margin: EdgeInsets.all(5),
                width: MediaQuery.of(context).size.width * 0.90,
                height: MediaQuery.of(context).size.height * 0.12,
                child: GestureDetector(
//                  behavior: HitTestBehavior.opaque,
                  onTap: () {
                    String surveyId =
                        snapshot.data[index]["survey_id"].toString();
                    MockSurveyPojo mockSurveyPojo = new MockSurveyPojo();
                    mockSurveyPojo.surveyId = surveyId;
                    mockSurveyPojo.surveyName =
                        snapshot.data[index]["survey_title"].toString();
                    mockSurveyPojo.totalQuestions =
                        snapshot.data[index]["total_questions"];
//                    Navigator.pushNamed(context, "/mock_survey",
//                        arguments: mockSurveyPojo);

//                    Navigator.of(context).push(route)

                    Navigator.of(context).push(PageTransition(
                        type: PageTransitionType.slideZoomLeft,
                        child: Instruction(mockSurveyPojo))
                    );


//                    Navigator.pushNamed(context, "/instruction",
//                        arguments: mockSurveyPojo);
                  },
                  child: Card(
//                    padding: EdgeInsets.all(20),
//                  margin: EdgeInsets.all(10),
                    color: Color(0xFFf3f3f3),
                    elevation: 2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
          Row(children: <Widget>[
           SizedBox(width: 30,),
            ///

            Text(
              snapshot.data[index]["survey_title"].toString(),
              style: TextStyle(
                  color: Color(0xFF525151),
                  fontSize: 18,
                  fontWeight: FontWeight.normal),
            ),

            ///
                    ],),

//                        Text(
//                          snapshot.data[index]["survey_title"].toString(),
//                          style: TextStyle(
//                              color: Color(0xFF525151),
//                              fontSize: 18,
//                              fontWeight: FontWeight.normal),
//                        ),
                      Row(children: <Widget>[
                        SizedBox(width: 30,),
                        Text(
                          subtitle,
                          style: TextStyle(
                              color: Color(0xFF525151),
                              fontSize: 14,
                              fontWeight: FontWeight.w500),
                        ),
                      ],),

                      ],
                    ),
                  ),
                ),
              );
            },
          );
        }
      },
    );
  }

  _buildSurveyListView() {
    return FutureBuilder(
      future: DBProvider.db.getallSurevey(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
//          return Text(context.toString());
//        Text(DBProvider.survey_name);
          return ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.black12,
            ),
            itemCount: snapshot.data.length,
//            Survey.fromJson(snapshot.data).toString().length,
//            snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                leading: Text(
                  "${index + 1}",
                  style: TextStyle(fontSize: 20.0),
                ),
                title: Column(
                  children: <Widget>[
                    Text(
                      snapshot.data[index]["surveyId"].toString(),
                    ),
                    Text(
                      snapshot.data[index]["surveyTitle"].toString(),
                    ),
                    Text(
                      snapshot.data[index]["startDate"].toString(),
                    ),
                    Text(
                      snapshot.data[index]["surveyDescription"].toString(),
                    ),
                    Text(
                      snapshot.data[index]["endDate"].toString(),
                    ),
                    Text(
                      snapshot.data[index]["v"].toString(),
                    ),
                  ],
                ),
              );
            },
          );
        }
      },
    );
  }

  ///function for choices
  choicessurvey() {
    return FutureBuilder(
      future: DBProvider.db.getallchoices(),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (!snapshot.hasData) {
          return Center(
            child: CircularProgressIndicator(),
          );
        } else {
//          return Text(context.toString());
//        Text(DBProvider.survey_name);
          return ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.black12,
            ),
            itemCount: snapshot.data.length,
//            Survey.fromJson(snapshot.data).toString().length,
//            snapshot.data.length,
            itemBuilder: (BuildContext context, int index) {
              return ListTile(
                leading: Text(
                  "${index + 1}",
                  style: TextStyle(fontSize: 20.0),
                ),
                title: Column(
                  children: <Widget>[
                    Text(
                      snapshot.data[index]["option_id"].toString(),
                    ),
                    Text(
                      snapshot.data[index]["question_id"].toString(),
                    ),
                    Text(
                      snapshot.data[index]["choices"].toString(),
                    ),
                    Text(
                      snapshot.data[index]["createdAt"].toString(),
                    ),
                  ],
                ),
              );
            },
          );
        }
      },
    );
  }
}
