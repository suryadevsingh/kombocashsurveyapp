

import 'package:flutter/material.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:surveyapp/Screens/Question3.dart';

class Question2 extends StatefulWidget {
  @override
  _Question2State createState() => _Question2State();
}

class _Question2State extends State<Question2> {

  List data ;
  int selectedRadioTile;
  setSelectedRadioTile(int val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  @override
  void initState() {
    super.initState();
//    selectedRadio = 0;
    selectedRadioTile = 0;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.height *0.08,),
              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height *0.04,
                  width: MediaQuery.of(context).size.width *0.75,
                  child: FAProgressBar(
                    backgroundColor: Colors.grey[300],
                    progressColor: Colors.greenAccent[700],
                    currentValue: 60,
//                  displayText: "%",
                  ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height *0.06,),
              Container(
                height: MediaQuery.of(context).size.height *0.20,
                width: MediaQuery.of(context).size.height *0.40,

                decoration: BoxDecoration(
                  color: Color(0xFFf3f3f3),
                  borderRadius:BorderRadius.all((Radius.circular((4.0))),

                  ),
                  border: Border.all(
                      color: Colors.grey[400],width: 2
                  ),
                ),
                /// question
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
//                  SizedBox(height: 27,),
                    Text("From where did you ", style: TextStyle(fontWeight: FontWeight.w600,fontSize: 22),),
                    SizedBox(height: 5,),
                    Text("bought this product ?", style: TextStyle(fontWeight: FontWeight.w600,fontSize: 22),),
                  ],
                ),
//        child:    FutureBuilder(builder: (context, snapshot){
//              var showData =json.decode(snapshot.data.toString());
//              return ListView.builder(
//                itemBuilder: (BuildContext context , int index){
//                  return
//                    Text(showData[0].question_title);
////                    subtitle: Text(showData[index]["height"]),
//
//                },
//                itemCount: showData.length,
//              );
//            },
//              future: DefaultAssetBundle.of(context).loadString("load_json/survey.json"),
//            ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height *0.06,),
//          Expanded(
//            child: FutureBuilder(builder: (context, snapshot){
//              var showData =json.decode(snapshot.data.toString());
//              return ListView.builder(
//                itemBuilder: (BuildContext context , int index){
//                  return RadioListTile(
//                  title: Text(showData[index]["option"]["options"]),
//                  activeColor: Colors.blue,
//                    groupValue: selectedRadioTile,
//                    onChanged: null,
//                    value: index,
//
//                  );
//                },
//                itemCount: showData.length,
//              );
//            },
//              future: DefaultAssetBundle.of(context).loadString("load_json/person.json"),
//            ),
//          ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width * 0.75,
                    height: MediaQuery.of(context).size.height *0.08,
                    child: RadioListTile(
                      value: 1,
                      title: Text("Online"),
                      activeColor: Colors.blue,
                      groupValue:selectedRadioTile,
                      onChanged: (val){
                        print("Radio Tile pressed $val");
                        setSelectedRadioTile(val);
                      },
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.75,
                    height: MediaQuery.of(context).size.height *0.08,
                    child: RadioListTile(
                      value: 2,
                      title: Text("Offline"),
                      activeColor: Colors.blue,
                      groupValue:selectedRadioTile,
                      onChanged: (val){
                        print("Radio Tile pressed $val");
                        setSelectedRadioTile(val);
                      },
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.75,
                    height: MediaQuery.of(context).size.height *0.08,
                    child: RadioListTile(
                      value: 3,
                      title: Text("From the workshop"),
                      activeColor: Colors.blue,
                      groupValue:selectedRadioTile,
                      onChanged: (val){
                        print("Radio Tile pressed $val");
                        setSelectedRadioTile(val);
                      },
                    ),
                  ),
                  Container(
                    width: MediaQuery.of(context).size.width * 0.75,
                    height: MediaQuery.of(context).size.height *0.08,
                    child: RadioListTile(
                      value: 4,
                      title: Text("I don't remember"),
                      activeColor: Colors.blue,
                      groupValue:selectedRadioTile,
                      onChanged: (val){
                        print("Radio Tile pressed $val");
                        setSelectedRadioTile(val);
                      },
                    ),
                  ),
                ],
              ),

              SizedBox(height: MediaQuery.of(context).size.height *0.08,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
//                SizedBox(width: MediaQuery.of(context).size.width * 0.09,),
                  OutlineButton(onPressed: (){
                    Navigator.pop(context);
                  },
                    borderSide: BorderSide(color: Colors.greenAccent),
                    child: Text("Back",style: TextStyle(color: Colors.greenAccent),),),
//                SizedBox(width: MediaQuery.of(context).size.width* 0.40,),
                  MaterialButton(
                    onPressed: (){
                      Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Question3()));
                    },
                    color: Colors.greenAccent,
                    child: Text("Next",style: TextStyle(color: Colors.white),),
                  ),
//                SizedBox(width: MediaQuery.of(context).size.width * 0.08,),
                ],
              ),

            ],
          ),
        ),
      ),
    );
  }
}


