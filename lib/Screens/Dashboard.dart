import 'dart:async';
import 'dart:convert';
import 'package:connectivity/connectivity.dart';
import 'package:fcharts/fcharts.dart';
import 'package:flutter/cupertino.dart';
import 'package:charts_flutter/flutter.dart' as charts;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:surveyapp/Screens/Dbtoui.dart';
import 'package:surveyapp/Screens/survey/pojo/MockSurveyPojo.dart';
import 'package:surveyapp/backend/db_provider.dart';
import 'package:surveyapp/backend/local_json.dart';
import 'package:syncfusion_flutter_charts/charts.dart';
import '../utils/Utility.dart';
import 'auth/Signin.dart';
import 'package:logger/logger.dart';

class Dashboard extends StatefulWidget {
  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  var isLoading = false;
  int availableSurvey = 0;
  int newSurvey = 0;
  List<charts.Series<Sales, int>> _seriesLineData;
  bool set = false;

  _generateData() {

    var linesalesdata = [
      new Sales(0, 45),
      new Sales(1, 56),
      new Sales(2, 55),
      new Sales(3, 60),
      new Sales(4, 61),
      new Sales(5, 70),
    ];
    var linesalesdata1 = [
      new Sales(0, 35),
      new Sales(1, 46),
      new Sales(2, 45),
      new Sales(3, 50),
      new Sales(4, 51),
      new Sales(5, 60),
    ];

    var linesalesdata2 = [
      new Sales(0, 20),
      new Sales(1, 24),
      new Sales(2, 25),
      new Sales(3, 40),
      new Sales(4, 45),
      new Sales(5, 60),
    ];

    _seriesLineData.add(
      charts.Series(
        colorFn: (__, _) => charts.ColorUtil.fromDartColor(Color(0xff990099)),
        id: 'Air Pollution',
        data: linesalesdata,
        domainFn: (Sales sales, _) => sales.yearval,
        measureFn: (Sales sales, _) => sales.salesval,
      ),
    );
    _seriesLineData.add(
      charts.Series(
        colorFn: (__, _) => charts.ColorUtil.fromDartColor(Color(0xff990099)),
        id: 'Air Pollution',
        data: linesalesdata1,
        domainFn: (Sales sales, _) => sales.yearval,
        measureFn: (Sales sales, _) => sales.salesval,
      ),
    );
    _seriesLineData.add(
      charts.Series(
        colorFn: (__, _) => charts.ColorUtil.fromDartColor(Color(0xff990099)),
        id: 'Air Pollution',
        data: linesalesdata2,
        domainFn: (Sales sales, _) => sales.yearval,
        measureFn: (Sales sales, _) => sales.salesval,
      ),
    );
  }


  ///connection setstate

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit the App?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () {
              SystemChannels.platform.invokeMethod('SystemNavigator.pop');
            },
            child: new Text('Yes'),
          ),
        ],
      ),
    ) ??

        false;

  }




  _checkInternetfroloadingsurveyConnectivity() async {
    var result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.none) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Please check your internet connectivity"),
        duration: Duration(seconds: 1),
        backgroundColor: Colors.red,
      ));
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Hold a second "),
        duration: Duration(seconds: 1),
        backgroundColor: Colors.green,
      ));
      _loadFromApi();
    }
  }

  _checkInternetConnectivity() async {
    var result = await Connectivity().checkConnectivity();
    if (result == ConnectivityResult.none) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("You're not connected to a network"),
        duration: Duration(milliseconds: 1200),
        backgroundColor: Colors.red,
      ));
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content:  Text("Your response has been saved",),
        duration: Duration(milliseconds: 1200 ),
        backgroundColor: Colors.green,
      ));
//      _scaffoldKey.currentState.showSnackBar(SnackBar(
//        content: Text("You're response has been send"),
//        duration: Duration(seconds: 1),
//        backgroundColor: Colors.green,
//      ));



//      uploadUserResponseData();

//      testuploadUserResponseData();

//      test1uploadUserResponseData();

//      uploadUserResponseData();


      ///code for deleting pervious responses


      setState(() async{
//        await Future.delayed(const Duration(seconds: 10));
        await test1uploadUserResponseData();
//        bool dosa =true;
          set = true ;
        deleteallperviousData();
        deleteallpervious1Data();
          fetchCountForSurveyresponse("responses");
        Future.delayed(const Duration(seconds: 7), () {
          setState(() {
            set =false;
//            set =true;
          });
        });
      });

    }

  }

  static const Responses = [
    ["Mon", "20"],
    ["Tue", "50"],
    ["Wed", "20"],
    ["Thu", "60"],
    ["Fri", "70"],
    ["Sat", "100"],
    ["Sun", "20"],
  ];

  @override
  void initState(){
    // TODO: implement initState
    super.initState();
    _loadFromApi();
    _seriesLineData = List<charts.Series<Sales, int>>();
    _generateData();
  }



  _loadFromApi() async {
    setState(() {
      isLoading = true;
    });
    var apiProvider = SurveyLocalJson();
    await apiProvider.loadAsset(apiProvider.Mydata);
    await Future.delayed(const Duration(seconds: 1));
    setState(() {
      isLoading = false;
    });
  }


  @override
  Widget build(BuildContext context) {
    _deleteData() async {
      setState(() {
        isLoading = true;
      });

      await DBProvider.db.deleteOnlyAllSurvey();
//    await DBProvider.db.getAllSurvey();

      // wait for 1 second to simulate loading of data
      await Future.delayed(const Duration(seconds: 1));

      setState(() {
        isLoading = false;
      });

      print('All employees deleted');
    }
    return WillPopScope(
//      onWillPop: () async => false,
      onWillPop:_onWillPop,
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: true,
//      backgroundColor: Colors.white,
        appBar: AppBar(
          elevation: 0,
          title: Text(
            "Dashboard",
            style: TextStyle(
                color: Color(0xFF002632),
                fontSize: 26,
                fontWeight: FontWeight.bold),
            textAlign: TextAlign.start,
          ),
          actions: <Widget>[

            IconButton(
                icon: Icon(
                  Icons.sync,
                  color: Colors.grey[400],
                ),
                onPressed: () {
                  _checkInternetConnectivity();
//                  uploadUserResponseData();
                })
          ],
          titleSpacing: 0,
        ),
        drawer: Drawer(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 10,
              ),
              Image.asset(
                "assets/Images/kombo_survey_logo.png",
                fit: BoxFit.contain,
                width: 220,
              ),
              InkWell(
                onTap: (){
                  _checkInternetfroloadingsurveyConnectivity();
                },
                child:    ListTile(
                  trailing: Icon(
                    CupertinoIcons.refresh_circled,
                    color: Color(0xFF8a8989),
                  ),
                  title: Text("Get survey",
                      style: TextStyle(fontSize: 15, color: Color(0xFF002632))),
                ),
              ),


              InkWell(
                onTap: (){

                  _checkInternetConnectivity();
                  Future.delayed(const Duration(seconds: 10), () {
                    setState(() {
                      set =false;
                    });
                  });
                },
                child:  ListTile(
                  trailing: Icon(
                    Icons.sync,
                    color: Colors.grey[400],
                  ),
                  subtitle: set ? Text("Your response has been saved",style: TextStyle(fontSize: 10),):
                 null,
                  title: Text("Send your response",
                      style: TextStyle(fontSize: 15, color: Color(0xFF002632))),
                ),
              ),

              InkWell(
                onTap: ()async {
                  await _deleteData();
                },
                child: ListTile(
                  trailing: Icon(
                    CupertinoIcons.delete,
                    color: Color(0xFF8a8989),
                  ),
                  title: Text("Delete all survey",
                      style: TextStyle(fontSize: 15, color: Color(0xFF002632))),
                ),
              ),
              InkWell(
                onTap: () {
                  userLogout();
                },
                child: ListTile(
                  title: Text(
                    "Log out",
                    style: TextStyle(fontSize: 15, color: Color(0xFF002632)),
                  ),
                  trailing: Icon(
                    Icons.input,
                    size: 20,
                  ),
                ),
              ),

              Text(" v1.9.0", style: TextStyle(fontSize: 12,color: Colors.red),),
            ],
          ),
        ),
        body: Container(
          color: Colors.white,
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.06,
              ),
              Center(
                child: GestureDetector(
                  onTap: () {
                    MockSurveyPojo mockSurveyPojo = new MockSurveyPojo();
                    mockSurveyPojo.surveyType = "Available";
                    mockSurveyPojo.totalQuestions = availableSurvey.toString();
                    Navigator.of(context).push(PageTransition(
                        type: PageTransitionType.slideZoomLeft,
                        child: HomePage(mockSurveyPojo)));

                    //                    Navigator.push(
//                      context,
//                      MaterialPageRoute(builder: (context) => AvailableSurveys()),
//                    );
                  },
                  child: Card(
                    elevation: 2,
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.90,
                      height: MediaQuery.of(context).size.height * 0.09,
                      color: Color(0xFFf3f3f3),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: 15,
                          ),
                          Center(
                              child: Text(
                            "Available Surveys",
                            style: TextStyle(
                                fontSize: 24,
                                color: Color(0xFFb81f6f),
                                fontWeight: FontWeight.w500),
                          ),
                          ),
                          SizedBox(
                            width: 40,
                          ),
                          fetchCountForAvailableSurvey("available")
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.026,
              ),
              Center(
                  child: GestureDetector(
                onTap: () {
                  MockSurveyPojo mockSurveyPojo = new MockSurveyPojo();
                  mockSurveyPojo.surveyType = "newly";
                  mockSurveyPojo.totalQuestions = availableSurvey.toString();
                  Navigator.of(context).push(PageTransition(
                      type: PageTransitionType.slideZoomLeft,
                      child: HomePage(mockSurveyPojo)));
                },
                child: Card(
                  elevation: 2,
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.90,
                    height: MediaQuery.of(context).size.height * 0.09,
                    color: Color(0xFFf3f3f3),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          width: 15,
                        ),
                        Center(
                            child: Text(
                          "Newly Added",
                          style: TextStyle(
                              fontSize: 24,
                              color: Color(0xFFb81f6f),
                              fontWeight: FontWeight.w500),
                        )),
                        SizedBox(
                          width: 85,
                        ),
                        fetchCountForAvailableSurvey("new"),

//                        SizedBox(width: 15,),
//                        Center(child: Text("4",style: TextStyle(fontSize: 40,color: Colors.black,fontWeight: FontWeight.w500),)),
//                        SizedBox(width:65,),
//                        Center(child: Text("Newly Added",style: TextStyle(fontSize: 26,color: Colors.deepPurple,fontWeight: FontWeight.w500),)),
                      ],
                    ),
                  ),
                ),
              )),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.026,
              ),
              Center(
                child: GestureDetector(
                  onTap: () {
//                    Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: HomePage()));
                  },
                  child: Card(
                    elevation: 2,
                    child: Container(
                      width: MediaQuery.of(context).size.width * 0.90,
                      height: MediaQuery.of(context).size.height * 0.09,
                      color: Color(0xFFf3f3f3),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          SizedBox(
                            width: 15,
                          ),
                          Center(
                            child: Text(
                              "Responses",
                              style: TextStyle(
                                  fontSize: 24,
                                  color: Color(0xFFb81f6f),
                                  fontWeight: FontWeight.w500),
                            ),
                          ),
                          SizedBox(

                            width: 120,
                          ),
                          fetchCountForSurveyresponse("responses"),
   ///
                          ///
//                          Center(
//                              child: Text(
//                            "200",
//                            style: TextStyle(
//                                fontSize: 35,
//                                color: Colors.black,
//                                fontWeight: FontWeight.w500),
//                          )),



///
                          ///
//                          SizedBox(width: 15,),
//                          Center(child: Text("200",style: TextStyle(fontSize: 35,color: Colors.black,fontWeight: FontWeight.w500),)),
//                          SizedBox(width: 25,),
//                          Center(child: Text("Responses",style: TextStyle(fontSize: 26,color: Colors.deepPurple,fontWeight: FontWeight.w500),),),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.03,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
//                  Container(
//                    margin: EdgeInsets.only(left: 30),
//                    child: Text(
//                      "Responses in this week",
//                      style: TextStyle(
//                          fontSize: 20,
//                          color: Color(0xFFb81f6f),
//                          fontWeight: FontWeight.w500),
//                    ),
//                  ),

                  ////graph
//                  Container(
//                    height: MediaQuery.of(context).size.height * 0.35,
//                    width: MediaQuery.of(context).size.width ,
//                    child: Center(
//                      child: Column(
//                        children: <Widget>[
//                          Expanded(
//                            child: charts.LineChart(
//                                _seriesLineData,
//                                defaultRenderer: new charts.LineRendererConfig(
//                                    includeArea: true, stacked: true),
//                                animate: true,
//                                animationDuration: Duration(seconds: 3),
//                                behaviors: [
//                                  new charts.ChartTitle('Surveys',
//                                      behaviorPosition: charts.BehaviorPosition.bottom,
//                                      titleOutsideJustification:charts.OutsideJustification.middleDrawArea),
//                                  new charts.ChartTitle('Responses',
//                                      behaviorPosition: charts.BehaviorPosition.start,
//                                      titleOutsideJustification: charts.OutsideJustification.middleDrawArea),
//                                ]
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                  ),
//                  Container(
//                    height: MediaQuery.of(context).size.height * 0.30,
//                    width: MediaQuery.of(context).size.width * 0.90,
//                    child: LineChart(
////                vertical: true,
//                      lines: [
//                        Line<List<String>, String, String>(
//                          data: Responses,
//                          xFn: (dataum) => dataum[0],
//                          yFn: (dataum) => dataum[1],
//                        ),
//                      ],
//                    ),
//                  ),

                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void userLogout() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs?.setBool("isLoggedIn", false);
    Navigator.of(context).push(PageTransition(
        type: PageTransitionType.slideZoomLeft, child: Signin()));
  }

  fetchCountForAvailableSurvey(String type) {
    return new FutureBuilder(
        future: fetchCountForAvailableSurveyFromDb(type),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return Center(
              child: Text(
            snapshot.data.toString(),
            style: TextStyle(
                fontSize: 40, color: Colors.black, fontWeight: FontWeight.w500),
          ));
        });
  }

  Future<int> fetchCountForAvailableSurveyFromDb(String type) async {
    var allSurvey;
    if (type == "available") {
      allSurvey = await DBProvider.db.getAllSurvey();
    } else {
      allSurvey = await DBProvider.db.fetchAllNewlyAddedSurvey();
    }
    return Future(() => allSurvey.length);
  }

///testing

  fetchCountForSurveyresponse(String type) {
    return new FutureBuilder(
        future: fetchCountForSurveyresponsesFromDb(type),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          return Center(
              child: Text(
                snapshot.data.toString(),
                style: TextStyle(
                    fontSize: 40, color: Colors.black, fontWeight: FontWeight.w500),
              ));
        });
  }

  Future<int> fetchCountForSurveyresponsesFromDb(String type) async {
    var allSurveyresponse;
    if (type == "Username") {
      allSurveyresponse = await DBProvider.db.getAllPersonalinfo();
    } else {
      allSurveyresponse = await DBProvider.db.getAllPersonalinfo();
    }
    return Future(() => allSurveyresponse.length);
  }

  ///

  void uploadUserResponseData() async {

    var allResponses = await DBProvider.db.fetchAllSurveyResponse();
    print(allResponses);
    print('allResponses');
    var allUsers = await DBProvider.db.fetchAllUsers();
    print(allUsers);
    print('allUsers');
    var data = [];
    List<String> uniqueSurveyIds = [];

    for (var i = 0; i < allResponses.length; i++) {

      if (!uniqueSurveyIds.contains(allResponses[i]["survey_id"])) {

        uniqueSurveyIds.add(allResponses[i]["survey_id"]);

      }
    }

      for (var i = 0; i < uniqueSurveyIds.length; i++) {

        var obj = {};

        obj["surveyId"] = uniqueSurveyIds[i];

        obj["surveyor_id"] = await Utility.getLoggedInUserEmail();

        var allUsers = await DBProvider.db.fetchAllUsersWithThisSurvey(uniqueSurveyIds[i]);

        print("alluser @1 ");

        print(allUsers);

        for (var j = 0; j < allUsers.length; j++) {

          obj["user_details"] = allUsers[j];

          String userId = allUsers[j]["user_id"];

          ///issue of retake
          ///
          var allResponses = await DBProvider.db.fetchAllSurveyResponseWhereUserIdAndSurveyId(userId, uniqueSurveyIds[i]);
         print("allResponses @1");

         print(allResponses);

         obj["response"] = allResponses;

        }
        data.add(obj);

      }

      print("jsonEncode data");
      print(jsonEncode(data));
      String urlString = Utility.PRODUCTION_URL + Utility.UPLOAD_SURVEY;
      print("string");

      Map<String, String> headers = {"Content-Type": "application/json"};
      Response response = await post(
          urlString, headers: headers, body: jsonEncode(data));

      var result = response.body.toString();
      var map = jsonDecode(result);
       print("map");
      print(map);

  }

  void testuploadUserResponseData() async {

    var allResponses = await DBProvider.db.fetchAllSurveyResponse();
//    print(allResponses);
    print('allResponses');
    var allUsers = await DBProvider.db.fetchAllUsers();
//    print(allUsers);
    print('allUsers');
    var data = [];
    List<String> uniqueSurveyIds = [];

    for (var i = 0; i < allResponses.length; i++) {
      if (!uniqueSurveyIds.contains(allResponses[i]["survey_id"])) {
        uniqueSurveyIds.add(allResponses[i]["survey_id"]);
      }
    }
    var obj = {};
    for (var i = 0; i < uniqueSurveyIds.length; i++) {
//      var obj = {};
        obj["surveyId"] = uniqueSurveyIds[i];
        obj["surveyor_id"] = await Utility.getLoggedInUserEmail();
//        obj["user_details"] = await DBProvider.db.fetchAllUsersWithThisSurvey(uniqueSurveyIds[i]);

//for(var i = 0 ; i < allUsers.length ;i++){

      obj["user_details"] = await DBProvider.db.fetchAllUsers();

//}
//        print("alluser @1 ");
//        print(allUsers);
//      print("alluser @obj ");
//        data.add(obj);

//        data.add(obj);

      for (var j = 0; j < allUsers.length; j++) {
        var obj = {};
        obj["user_details"] = allUsers[j];
        String userId = allUsers[j]["user_id"];
        ///issue of retake

//          obj["response"] = await DBProvider.db.fetchAllSurveyResponseWhereUserIdAndSurveyId(userId, uniqueSurveyIds[i]);

        obj["response"]= await DBProvider.db.fetchAllSurveyResponse();

        //          print("dosa");

//print( obj["response"]);

//          print("allResponses @1");
//          print(allResponses);
//          obj["response"] = allResponses;
        data.add(obj);
      }
      data.add(obj);
      }

    var logger = Logger(
      printer: PrettyPrinter(
          methodCount: 2, // number of method calls to be displayed
          errorMethodCount: 8, // number of method calls if stacktrace is provided
          lineLength: 2000, // width of the output
          colors: true, // Colorful log messages
          printEmojis: true, // Print an emoji for each log message
          printTime: false // Should each log print contain a timestamp
      ),
    );
      print("jsonEncode data");
    logger.v(jsonEncode(data));
      String urlString = Utility.PRODUCTION_URL + Utility.UPLOAD_SURVEY;
      print("string");

      Map<String, String> headers = {"Content-Type": "application/json"};
      Response response = await post(
          urlString, headers: headers, body: jsonEncode(data));

      var result = response.body.toString();
      var map = jsonDecode(result);
       print("map");
      print(map);

  }



   test1uploadUserResponseData() async {

    var allResponses = await DBProvider.db.fetchAllSurveyResponse();
//    print(allResponses);
    print('allResponses');
    var allUsers = await DBProvider.db.fetchAllUsers();
//    print(allUsers);
    print('allUsers');
    var data = [];
    List<String> uniqueSurveyIds = [];

    for (var i = 0; i < allResponses.length; i++) {
      if (!uniqueSurveyIds.contains(allResponses[i]["survey_id"])) {


        uniqueSurveyIds.add(allResponses[i]["survey_id"]);
      }
      print(allResponses.length);
    }


    var obj = {};
    obj["surveyId"] = uniqueSurveyIds;
    obj["surveyor_id"] = await Utility.getLoggedInUserEmail();
    obj["user_details"] = await DBProvider.db.fetchAllUsers();
    print("user @@@@@1");
//    print(obj["user_details"]);

//    data.add(obj);

//    for (var j = 0; j < allUsers.length; j++) {
//      var obj = {};
//      obj["user_details"] = allUsers;
//      String userId = allUsers["user_id"];
      ///issue of retake

//          obj["response"] = await DBProvider.db.fetchAllSurveyResponseWhereUserIdAndSurveyId(userId, uniqueSurveyIds[j]);

      obj["response"]= await DBProvider.db.fetchAllSurveyResponse();

      //          print("dosa");

//print( obj["response"]);

          print("allResponses @1");
          print(obj["response"]);
//          obj["response"] = allResponses;
      data.add(obj);
//    }

//    var obj = {};
//    for (var k = 0; k < uniqueSurveyIds.length; k++) {
////      var obj = {};
//
////        obj["user_details"] = await DBProvider.db.fetchAllUsersWithThisSurvey(uniqueSurveyIds[i]);
//
////for(var i = 0 ; i < allUsers.length ;i++){
//
//      obj["surveyId"] = uniqueSurveyIds[k];
//      obj["surveyor_id"] = await Utility.getLoggedInUserEmail();
//      if (!uniqueSurveyIds.contains(allUsers[k]["user_id"])) {
//
////        obj["user_details"] = await DBProvider.db.fetchAllUsersWithThisSurvey(uniqueSurveyIds[i]);
//        obj["user_details"] = await DBProvider.db.fetchAllUsers();
//        print("user");
//        print(obj["user_details"]);
//        data.add(obj);
//        obj["user_details"] = await DBProvider.db.fetchAllUsers();
//
//        for (var j = 0; j < allUsers.length; j++) {
//
//
////          var obj = {};
//
//          obj["user_details"] = allUsers[i];
//          String userId = allUsers[i]["user_id"];
//
//          ///issue of retake
//
//          obj["response"] = await DBProvider.db.fetchAllSurveyResponseWhereUserIdAndSurveyId(userId, uniqueSurveyIds[i]);
//
////          obj["response"]= await DBProvider.db.fetchAllSurveyResponse();
//            print("responsess");
//            print(obj["response"]);
////          data.add(obj);
//        }
//      }
//      data.add(obj);
//      obj["user_details"] = await DBProvider.db.fetchAllUsers();


//}
//        print("alluser @1 ");
//        print(allUsers);
//      print("alluser @obj ");
//        data.add(obj);

//        data.add(obj);



//      }

    var logger = Logger(
      printer: PrettyPrinter(
          methodCount: 2, // number of method calls to be displayed
          errorMethodCount: 8, // number of method calls if stacktrace is provided
          lineLength: 2000, // width of the output
          colors: true, // Colorful log messages
          printEmojis: true, // Print an emoji for each log message
          printTime: false // Should each log print contain a timestamp
      ),
    );
      print("jsonEncode data");
    logger.v(jsonEncode(data));
      String urlString = Utility.PRODUCTION_URL + Utility.UPLOAD_SURVEY;
      print("string");

      Map<String, String> headers = {"Content-Type": "application/json"};
      Response response = await post(
          urlString, headers: headers, body: jsonEncode(data));

      var result = response.body.toString();
      var map = jsonDecode(result);
       print("map");
      print(map);

      return response;

  }



  deleteallperviousData() async {
    setState(() {
      isLoading = true;
      bool set = true ;
    });
    await DBProvider.db.deleteAllperviousSurveyresponses();
//    await DBProvider.db.getAllSurvey();

    // wait for 1 second to simulate loading of data
    await Future.delayed(const Duration(seconds: 1));

    setState(() {
      isLoading = false;
    });

    print('All employees deleted');
  }
  deleteallpervious1Data() async {
    setState(() {
      isLoading = true;
      bool set = true ;
    });
    await DBProvider.db.deleteAllPreviousOptionsStored();
//    await DBProvider.db.getAllSurvey();

    // wait for 1 second to simulate loading of data
    await Future.delayed(const Duration(seconds: 1));

    setState(() {
      isLoading = false;
    });

    print('All employees deleted');
  }
}

class Sales {
  int yearval;
  int salesval;

  Sales(this.yearval, this.salesval);
}