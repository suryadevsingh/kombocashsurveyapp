
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:http/http.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

class ForgotPassword extends StatefulWidget {
  @override
  _ForgotPasswordState createState() => _ForgotPasswordState();
}

class _ForgotPasswordState extends State<ForgotPassword> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final _text = TextEditingController();
  Map<String, dynamic> map;
  static final Create_post_url ="http://kombocash.com/forgotPasswordLink";

  RegExp regex = new RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
  Future<void> postDataAPI() async {
    String urlString = Create_post_url;
    Map<String, String> headers = {"Content-Type": "application/json"};
    String email =  _text.text;
    String rawString =
        '{"email": "$email"}';

    Response response = await post(urlString, headers: headers, body: rawString);
    int statusCode = response.statusCode;
    print('statusCode = $statusCode');
    print('response.body = ${response.body}');

    var result = response.body;

    map = jsonDecode(result);
    print(map["success"]);
    print("Success");
    if(!regex.hasMatch(email)){
      Fluttertoast.showToast(
          msg: "please enter valid email ",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
//          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 15.0
      );
    }
    else if(map["success"]) {
      Fluttertoast.showToast(
          msg: "Please check your email",
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
//          timeInSecForIos: 1,
          backgroundColor:Color(0xFFb81f6f),
          textColor: Colors.white,
          fontSize: 15.0
      );

//      Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Dashboard()));

    }  else {
      Fluttertoast.showToast(
          msg: map["message"],
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.CENTER,
//          timeInSecForIos: 1,
          backgroundColor: Colors.red,
          textColor: Colors.white,
          fontSize: 15.0
      );
    }

  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async => false,
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Stack(
                overflow: Overflow.visible,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height * 0.28,
                    width: double.infinity,
                    child: WaveWidget(
                      config: CustomConfig(
                        colors: [
                          Colors.white70,
                          Colors.white54,
                          Colors.white30,
                          Colors.white24,
                        ],
                        durations: [32000, 21000, 18000, 5000],
                        heightPercentages: [0.75, 0.56, 0.58, 0.31],
                      ),
                      backgroundColor: Color(0xFFb81f6f),
                      size: Size(double.infinity,double.infinity),
                    ),
                  ),
                  Positioned(
                      top: 145,
                      left: 37,
                      child: Text("Forgot Password",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold),)),

                ],
              ),


              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height *0.70,
                  width: MediaQuery.of(context).size.width * 0.79,
//          color: Colors.white,
//      margin: EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      SizedBox(height: MediaQuery.of(context).size.height *0.10,),
//          SizedBox(height:20),
                      Text(" Email",style: TextStyle(fontWeight: FontWeight.bold),),
                      SizedBox(height: 5,),
                      Center(
                        child: Container(
//                        height: MediaQuery.of(context).size.height*0.075,
//                        width: MediaQuery.of(context).size.width *0.77,
                          decoration: BoxDecoration(
                            borderRadius:BorderRadius.all((Radius.circular((4.0))),
                            ),
                            border: Border.all(
                              color: Colors.grey[400],width: 2,
                            ),
                          ),
                          child: TextFormField(
                            controller: _text,
                            cursorColor: Colors.black,
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: "example@website.com",
                              hintStyle: TextStyle(fontSize: 13,color:Colors.black26),
                              prefixIcon: Icon(CupertinoIcons.mail,color: Colors.grey[700],size: 20,),
//                    focusedBorder: UnderlineInputBorder(
//                        borderSide: BorderSide(color: Colors.grey[400])
//                    ),
                            ),
                          ),
                        ),
                      ),

                      SizedBox(height: MediaQuery.of(context).size.height *0.4,),

//                    Center(
//                      child: InkWell(
//                        onTap: () {
//                          setState(() {
//                            if(_text.text.isEmpty){
//                              Fluttertoast.showToast(
//                                  msg: "Please enter email id",
//                                  toastLength: Toast.LENGTH_SHORT,
//                                  gravity: ToastGravity.CENTER,
//                                  timeInSecForIos: 1,
//                                  backgroundColor: Colors.grey[400],
//                                  textColor: Colors.white,
//                                  fontSize: 15.0
//                              );
//                            }
//                            else
//                            {
//                              postDataAPI();
////                        Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Dashboard()));
//                            }
//
//                          });
//                        },
//                        child: Container(
//                          height: MediaQuery.of(context).size.height * 0.06,
//                          width: MediaQuery.of(context).size.width * 0.27,
//                          decoration: BoxDecoration(
//                            borderRadius:BorderRadius.all((Radius.circular((4.0))),
//                            ),
//
//                          ),
//                          child: Card(
//                            color:Colors.greenAccent,
//                            shape: RoundedRectangleBorder(
//                              side: BorderSide(color: Colors.greenAccent, width: 1),
//                              borderRadius: BorderRadius.circular(4.0),
//                            ),
//                            child: Center(child: Text("Send ",style: TextStyle(color: Colors.white),),),
//                          ),
//                        ),
//                      ),
//                    ),

                      Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[

                          OutlineButton(onPressed: (){
                            Navigator.of(context).pop();
                          },
                            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                            borderSide: BorderSide(color: Color(0xFFb81f6f)),
                            child: Text("Back",style: TextStyle(color: Color(0xFFb81f6f)),),),
                          MaterialButton(
                            shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                            onPressed: (){
                              setState(() {
                                if(_text.text.isEmpty){

                                  _scaffoldKey.currentState.showSnackBar(SnackBar(
                                    content: Text("Please enter email id"),
                                    duration: Duration(seconds: 1),
                                    backgroundColor: Colors.red,
                                  ));

//                                  Fluttertoast.showToast(
//                                      msg: "Please enter email id",
//                                      toastLength: Toast.LENGTH_SHORT,
//                                      gravity: ToastGravity.CENTER,
//                                      timeInSecForIos: 1,
//                                      backgroundColor: Colors.red,
//                                      textColor: Colors.white,
//                                      fontSize: 15.0
//                                  );
                                }
                                else
                                {
                                  _scaffoldKey.currentState.showSnackBar(SnackBar(
                                    content: Text("Please check your email"),
                                    duration: Duration(seconds: 1),
                                    backgroundColor: Colors.green,
                                  ));

//                                  Fluttertoast.showToast(
//                                      msg: "Check your email",
//                                      toastLength: Toast.LENGTH_SHORT,
//                                      gravity: ToastGravity.CENTER,
//                                      timeInSecForIos: 1,
//                                      backgroundColor: Color(0xFFb81f6f),
//                                      textColor: Colors.white,
//                                      fontSize: 15.0
//                                  );
                                  postDataAPI();
//                        Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Dashboard()));
                                }

                              });
                              },
                            elevation: 1,
                            color: Color(0xFFb81f6f),
                            child: Text("Send",style: TextStyle(color: Colors.white),),

                          ),
                        ],
                      )



                    ],
                  ),

                ),
              ),

            ],


          ),
        ),
      ),
    );
  }
}
