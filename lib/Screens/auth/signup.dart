import 'dart:convert';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:flutter_page_transition/page_transition_type.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'package:passwordfield/passwordfield.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:surveyapp/Modelclass/signUpmodelclass.dart';
import 'package:surveyapp/Screens/Dashboard.dart';
import 'package:surveyapp/Screens/auth/Signin.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';
import 'package:fluttertoast/fluttertoast.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();


  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit the App?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () {
              SystemChannels.platform.invokeMethod('SystemNavigator.pop');
            },
            child: new Text('Yes'),
          ),
        ],
      ),
    ) ??
        false;
  }

  Map<String, dynamic> map;
  bool passwordVisible;
  bool isApiCalling =false;
  FocusNode textSecondFocusNode = new FocusNode();
  FocusNode textthirdFocusNode = new FocusNode();
  Pattern pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regex = new RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
  RegExp regex1 = new RegExp("^(?=.*[A-Z])(?=.*[%!@#*&])(?=.*[0-9])(?=.*[a-z].*[a-z]).{8}" );

//  static final Create_post_url ="https://komb-cash-mean.herokuapp.com/signUp";
  static final Create_post_url ="http://kombocash.com/signUp";
var response ;
  final _text = TextEditingController();
  final _text1 = TextEditingController();
  final _text2 = TextEditingController();
  bool _validate = false;
  bool _isChecked = true;


  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String email;
  String password;


  @override
  void initState() {
    passwordVisible = true;

  }


  Future<SignUp> postDataAPI() async {
    String urlString = Create_post_url;
    Map<String, String> headers = {"Content-Type": "application/json"};
  String userName = _text.text;
  String email =  _text1.text;
  String password = _text2.text;

    String rawString =
        '{"userName": "$userName", "email": "$email" ,"password":"$password"}';
    Response response = await post(urlString, headers: headers, body: rawString );
//    int statusCode = response.statusCode;
//    print('statusCode = $statusCode');
    print('response.body = ${response.body}');
    var result = response.body.toString();
    isApiCalling = false;
    setState(() {

    });
    map = jsonDecode(result);

    print(map["success"]);
    print("Success");
//    if(map["success"]){
    if(!regex.hasMatch(email)){
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Please enter valid email"),
        duration: Duration(seconds: 1),
        backgroundColor: Colors.red,
      ),);

    }
   else
     if(!regex1.hasMatch(password)) {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Password must contain one capital letter, two small letters, one digit and a special character and at least 8 characters in length",
        ),
        duration: Duration(seconds: 1),
        backgroundColor: Colors.red,
      ));
    }
    else if(_isChecked == false){
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text("Please agree terms and condtions"),
        duration: Duration(seconds: 1),
        backgroundColor: Colors.red,
      ));
    }

    else if (map["success"]){
      SharedPreferences prefs = await SharedPreferences.getInstance();
      prefs?.setBool("isLoggedIn", true);
      prefs?.setString("userEmail", email);
      print(map["data"]["userName"]);
      print("case 1");
      Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Dashboard()));

    }
    else{
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: Text(map["message"]),
        duration: Duration(seconds: 1),
        backgroundColor: Colors.red,
      ));
    }
  }

  String validateEmail(String value) {
    Pattern pattern1 ="^(?=.*[A-Z])(?=.*[%!@#*&])(?=.*[0-9])(?=.*[a-z].*[a-z]).{8}" ;
//    Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    RegExp regex1 = new RegExp(pattern1);
    if (!regex.hasMatch(value))
      return 'Enter Valid Email';
    else
      return null;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop:_onWillPop,
      child: Scaffold(
          key: _scaffoldKey,
          resizeToAvoidBottomPadding: false,
          resizeToAvoidBottomInset: true,
          body: SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Stack(
                  overflow: Overflow.visible,
                  children: <Widget>[

            Container(
              height: MediaQuery.of(context).size.height * 0.28,
              width: double.infinity,
              child: WaveWidget(
                config: CustomConfig(
                  colors: [
                    Colors.white70,
                    Colors.white54,
                    Colors.white30,
                    Colors.white24,
                  ],
                  durations: [32000, 21000, 18000, 5000],
                  heightPercentages: [0.75, 0.56, 0.58, 0.31],
                ),
                backgroundColor: Color(0xFFb81f6f),
                size: Size(double.infinity,double.infinity),
              ),
            ),
                    Positioned(
                      top: 130,
                      left: 50,
                        child: Text("Sign up",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Color(0xFF002632)),
                        ),
                    ),
                  ],
                ),

                SizedBox(   height: MediaQuery.of(context).size.height * 0.03,),
                Center(
                  child: Container(
                    height: MediaQuery.of(context).size.height *0.60,
                    width: MediaQuery.of(context).size.width * 0.77,
//                  color: Colors.white,
                    margin: EdgeInsets.all(20.0),
                    child: Form(
                        child:  Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text("Name",style: TextStyle(fontWeight: FontWeight.bold,color: Color(0xFF002632)),),
                            SizedBox(height:5,),
                            Container(
//                              height: MediaQuery.of(context).size.height*0.0730,
//                              width: MediaQuery.of(context).size.width *0.77,
                              decoration: BoxDecoration(
                                borderRadius:BorderRadius.all((Radius.circular((4.0))),
                                ),
                                border: Border.all(
                                    color: Color(0xFFdbdbdb),width: 1),
                              ),
                              child: TextFormField(
                                onFieldSubmitted: (String value) {
                                  FocusScope.of(context).requestFocus(textSecondFocusNode);
                                },
                                controller: _text,
//                                  validator: validateEmail,
                                cursorColor: Colors.black,
                                textInputAction: TextInputAction.next,
//                    enabled: true,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "John Doe",
                                  hintStyle: TextStyle(fontSize: 13,color:Colors.black26),
                                  prefixIcon: Icon(CupertinoIcons.person,color: Color(0xFF8a8989),size: 20,),
//                                  focusedBorder: UnderlineInputBorder(
//                                    borderSide: BorderSide(color: Colors.grey[400]),
//                                  ),
                                ),
//                              validator:
                              ),

                            ),

                            SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
                            Text("Email",style: TextStyle(fontWeight: FontWeight.bold,color: Color(0xFF002632)),),
                            SizedBox(height:5,),
                            Container(

//                              height: MediaQuery.of(context).size.height*0.0730,
//                              width: MediaQuery.of(context).size.width *0.77,
                              decoration: BoxDecoration(

                                color: Colors.transparent,
                                borderRadius:BorderRadius.all((Radius.circular((4.0))),
                                ),
                                border: Border.all(
                                    color:  Color(0xFFdbdbdb),width: 1
                                ),
                              ),
                              child: TextFormField(
                                onFieldSubmitted: (String value) {
                                  FocusScope.of(context).requestFocus(textthirdFocusNode);
                                },
                                focusNode: textSecondFocusNode,
                                controller: _text1,
                                textInputAction: TextInputAction.next,
                                cursorColor: Colors.black,
                                decoration: InputDecoration(

                                  hintText: "example@website.com",
                                  hintStyle: TextStyle(fontSize: 13,color:Colors.black26),
//                                errorText: " ",

                                  prefixIcon: Icon(CupertinoIcons.mail,color: Color(0xFF8a8989),size: 20,),
                                  border: InputBorder.none,
                                ),
                                validator: validateEmail,
                                keyboardType: TextInputType.emailAddress,

                              ),
                            ),
//                          TextFormField(
//                            decoration: InputDecoration(
//                              focusedErrorBorder: OutlineInputBorder(
//                                borderSide: BorderSide(
//                                  color: Colors.grey[400],
//                                ),
//                                borderRadius: BorderRadius.circular(10.0),
//                              ),
//                              disabledBorder: OutlineInputBorder(
//                                borderSide: BorderSide(
//                                  color: Colors.grey[400],
//                                ),
//                                borderRadius: BorderRadius.circular(10.0),
//                              ),
//                              focusedBorder:OutlineInputBorder(
//                                borderSide: BorderSide(
//                                  color: Colors.grey[400],
//                                ),
//                                borderRadius: BorderRadius.circular(10.0),
//                              ) ,
//                              border:OutlineInputBorder(
//                                borderSide: BorderSide(
//                                  color: Colors.grey[400],
//                                ),
//                                borderRadius: BorderRadius.circular(10.0),
//                              ) ,
//                              enabledBorder: OutlineInputBorder(
//                                borderSide: BorderSide(
//                                  color: Colors.grey[400],
//                                ),
//                                borderRadius: BorderRadius.circular(10.0),
//                              ),
//                                errorBorder: OutlineInputBorder(
//                                  borderSide: BorderSide(
//                                    color: Colors.red,
//                                  ),
//                                  borderRadius: BorderRadius.circular(10.0),
//                                ),
//                                hintText: "Error decoration text ...",
//                                errorText: "Ooops, something is not right!",
//                                errorStyle: TextStyle(
//                                    color: Colors.red, fontWeight: FontWeight.bold)),
//                          validator: (value){
//                              if (value.isEmpty) {
//                                return "Oops ,something is not right !";
//                              }
//                              return null;
//                          },
//                          ),
                            SizedBox(height: MediaQuery.of(context).size.height * 0.02,),
                            Text("Password",style: TextStyle(fontWeight: FontWeight.bold),),
                            SizedBox(height:5,),
                            Center(
                              child: Container(
//                                height: MediaQuery.of(context).size.height*0.10,
                                decoration: BoxDecoration(

                                  borderRadius:BorderRadius.all((Radius.circular((4.0))),
                                  ),
                                  border: Border.all(
                                    color:  Color(0xFFdbdbdb),width: 1,
                                  ),
                                ),
                                child: TextFormField(
                                  focusNode: textthirdFocusNode,
                                  keyboardType: TextInputType.text,
                                  controller: _text2,
                                  obscureText: passwordVisible,//This will obscure text dynamically
                                  decoration: InputDecoration(
                                    border: InputBorder.none,
//                    labelText: 'Password',
                                    prefixIcon: Icon(CupertinoIcons.padlock,color:Color(0xFF8a8989),size: 20,),
                                    hintText: 'Enter your password',
                                    hintStyle: TextStyle(fontSize: 13,color:Colors.black26),
                                    // Here is key idea
                                    suffixIcon: IconButton(
                                      icon: Icon(
                                        // Based on passwordVisible state choose the icon
                                        passwordVisible
                                            ? Icons.visibility_off
                                            : CupertinoIcons.eye_solid,
                                        color: Color(0xFF8a8989),
                                        size: 20,
                                      ),
                                      onPressed: () {
                                        // Update the state i.e. toogle the state of passwordVisible variable
                                        setState(() {
                                          passwordVisible = !passwordVisible;
                                        });
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            ),

//                          Container(
//                            height: 50,
//                            width: 280,
//                            decoration: BoxDecoration(
//                              borderRadius:BorderRadius.all((Radius.circular((4.0))),
//
//                              ),
//
//                              border: Border.all(
//                                color: Colors.grey[400],width: 2,
//                              ),
//                            ),
//                            child: TextField(
//                              cursorColor: Colors.black,
//                              decoration: InputDecoration(
////                                    hintText: hintText,
////                                    prefixIcon: hintText == "email"? Icon(Icons.vpn_key,color: Colors.grey[700]): Icon(Icons.lock),
////                                    suffixIcon: hintText == "Password" ? IconButton(icon: _isHidden ? Icon(Icons.visibility_off): Icon(Icons.visibility),
////                                        onPressed: _toggleVisibility) : null,
//                                focusedBorder: UnderlineInputBorder(
//                                    borderSide: BorderSide(color: Colors.grey[400])
//                                ),
//
//                              ),
//                              obscureText: hintText == "Password" ? _isHidden : false,
//                            ),
//                          ),
//                            SizedBox(height: MediaQuery.of(context).size.height * 0.02,),

                            Center(
                              child: Row(
                                children: <Widget>[
                                  Checkbox(
//                                  materialTapTargetSize: MaterialTapTargetSize.padded,
                                    value: _isChecked,
                                    activeColor: Colors.grey,
                                    checkColor: Colors.white,
                                    onChanged: (bool val) {
                                      setState(() {
                                        _isChecked = val;
                                        print(_isChecked);
                                      });
                                    }

                                  ),
                                  Text("Agree with Terms and Conditions",style: TextStyle(fontWeight: FontWeight.w500,fontSize: 13),),
                                ],
                              ),
                            ),
                            SizedBox(height: MediaQuery.of(context).size.height * 0.02,),

//                            Center(
//                              child: InkWell(
//                                onTap: (){
//                                  setState(() {
//                                    if(_text.text.isEmpty){
//                                      Fluttertoast.showToast(
//                                          msg: "Please enter name ",
//                                          toastLength: Toast.LENGTH_SHORT,
//                                          gravity: ToastGravity.CENTER,
//                                          timeInSecForIos: 1,
//                                          backgroundColor: Colors.grey[400],
//                                          textColor: Colors.white,
//                                          fontSize: 15.0
//                                      );
//                                    }
//                                    else if(_text1.text.isEmpty) {
//                                      Fluttertoast.showToast(
//                                          msg: "Please enter email",
//                                          toastLength: Toast.LENGTH_SHORT,
//                                          gravity: ToastGravity.CENTER,
//                                          timeInSecForIos: 1,
//                                          backgroundColor: Colors.grey[400],
//                                          textColor: Colors.white,
//                                          fontSize: 15.0
//                                      );
//                                    }
////                                    else if (_text2.text.length < 8){
////                                      Fluttertoast.showToast(
////                                          msg: "Password must contain atleast 8 characters",
////                                          toastLength: Toast.LENGTH_SHORT,
////                                          gravity: ToastGravity.CENTER,
////                                          timeInSecForIos: 1,
////                                          backgroundColor: Colors.grey[400],
////                                          textColor: Colors.white,
////                                          fontSize: 15.0
////                                      );
//
////                                    }
//                                    else if(_text2.text.isEmpty){
//                                      Fluttertoast.showToast(
//                                          msg: "Please enter password",
//                                          toastLength: Toast.LENGTH_SHORT,
//                                          gravity: ToastGravity.CENTER,
//                                          timeInSecForIos: 1,
//                                          backgroundColor: Colors.grey[400],
//                                          textColor: Colors.white,
//                                          fontSize: 15.0
//                                      );
//                                    }
//
//                                    else
//                                    {
//                                      postDataAPI();
//                                     }
//
//
//                                  });
//
//                                },
//                                child: Container(
//                                  height: MediaQuery.of(context).size.height * 0.06,
//                                  width: MediaQuery.of(context).size.width * 0.27,
//                                  decoration: BoxDecoration(
//                                    borderRadius:BorderRadius.all((Radius.circular((4.0))),
//                                    ),
//
//                                  ),
//                                  child: Card(
//
//                                    color:Colors.greenAccent,
//                                    shape: RoundedRectangleBorder(
//                                      side: BorderSide(color: Colors.greenAccent, width: 1),
//                                      borderRadius: BorderRadius.circular(4.0),
//                                    ),
//                                    child: Center(child: Text("Sign up",style: TextStyle(color: Colors.white),),),
//                                  ),
//                                ),
//                              ),
//                            ),

                            Visibility(
                                visible: isApiCalling,
                                child:  Center(
                                  child: SizedBox(
                                    height: 40,
                                    width: 40,
                                    child: CircularProgressIndicator(),
                                  ),
                                )

                            ),
                            Visibility(
                              visible: !isApiCalling,
                              child: Center(
                                child: MaterialButton(
                                  shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                                  minWidth: 35,
                                  height: MediaQuery.of(context).size.height * 0.0533,
                                  onPressed: (){
                                    setState(() {
                                      if(_text.text.isEmpty){
                                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                                          content: Text("Please enter name"),
                                          duration: Duration(seconds: 1),
                                          backgroundColor: Colors.red,
                                        ));
                                      }
                                      else if(_text1.text.isEmpty) {
                                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                                          content: Text("Please enter email"),
                                          duration: Duration(seconds: 1),
                                          backgroundColor: Colors.red,
                                        ));
                                      }
                                      else if(_text2.text.isEmpty){
                                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                                          content: Text("Please enter password"),
                                          duration: Duration(seconds: 1),
                                          backgroundColor: Colors.red,
                                        ));
                                      }
                                     else if(!regex.hasMatch(_text1.value.text)){
                                      _scaffoldKey.currentState.showSnackBar(SnackBar(
                                        content: Text("Please enter valid email"),
                                        duration: Duration(seconds: 1),
                                        backgroundColor: Colors.red,
                                      ),);

    }
                                      else if(_text2.text.length < 8 && !regex1.hasMatch(_text2.value.text)){
                                        _scaffoldKey.currentState.showSnackBar(SnackBar(
                                          content: Text("Password must contain one capital letter, two small letters, one digit and a special character and at least 8 characters in length"),
                                          duration: Duration(seconds: 1),
                                          backgroundColor: Colors.red,
                                        ));
                                      }
                                      else
                                      {
                                        isApiCalling = true;
                                        setState(() {

                                        });
                                        postDataAPI();
                                      }


                                    });
                                  },
                                  color: Color(0xFFb81f6f),
                                  child:Text("Sign up",style: TextStyle(color: Colors.white),) ,
                                ),
                              ),
                            ),
                            SizedBox(height: MediaQuery.of(context).size.height * 0.08,),
                            GestureDetector(
                                onTap: (){
                                  Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Signin()));
                                },
                                child:Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: <Widget>[
                                Text("Already a member ?",style: TextStyle(fontSize: 15,color:Color(0xFF002632) ,fontWeight: FontWeight.w600,),),
                                Text(" Sign in",style: TextStyle(fontSize: 15,color:Color(0xFFb81f6f) ,fontWeight: FontWeight.w600,decoration: TextDecoration.underline,),),

                          ],
                        )
                    ),
]
                  ),
                ),
                  ),
                ),
                  ],
                             ),
          )
      ),
    );
  }
}