
import 'dart:convert';
import 'dart:core';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart';
import 'package:passwordfield/passwordfield.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:surveyapp/Screens/Dashboard.dart';
import 'package:wave/config.dart';
import 'package:wave/wave.dart';

import 'ForgotPassword.dart';
import 'signup.dart';


class Signin extends StatefulWidget {
  @override
  _SigninState createState() => _SigninState();
}

class _SigninState extends State<Signin> {
  Map<String, dynamic> map;
  FocusNode textSecondFocusNode = new FocusNode();
  FocusNode textthirdFocusNode = new FocusNode();
//  RegExp regex1 = new RegExp("^(?=.*[!@#*&])(?=.*[0-9]).{8}");
  RegExp regex1 = new RegExp("^(?=.*[A-Z])(?=.*[!@#*&])(?=.*[0-9])(?=.*[a-z].*[a-z]).{8}");
  RegExp regex = new RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');
//  static final Create_post_url ="https://komb-cash-mean.herokuapp.com/signIn";
  static final Create_post_url ="http://kombocash.com/signIn";

  final _text = TextEditingController();
  final _text1 = TextEditingController();
  bool _validate = false;
  bool _isChecked = true;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String email;
  String password;
  bool passwordVisible;
  bool isApiCalling =false;
  String hintText ;

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit the App?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () {
              SystemChannels.platform.invokeMethod('SystemNavigator.pop');
            },
            child: new Text('Yes'),
          ),
        ],
      ),
    ) ??
        false;
  }


  @override
  void initState() {
    passwordVisible = true;

    checkIfLoggedIn();
  }
  void checkIfLoggedIn() async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    if(prefs.getBool("isLoggedIn") == true){
      Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Dashboard()));
    }
  }


  Future<void> postDataAPI() async {
    String urlString = Create_post_url;
    Map<String, String> headers = {"Content-Type": "application/json"};
    String email =  _text.text.toLowerCase();
    String password = _text1.text;
    String rawString =
        '{"email": "$email", "password": "$password"}';

    Response response = await post(urlString, headers: headers, body: rawString);
    int statusCode = response.statusCode;
    print('statusCode = $statusCode');
    print('response.body = ${response.body}');

    var result = response.body;
    isApiCalling = false;
    setState(() {

    });
    map = jsonDecode(result);

  print("Success");
  if(!regex.hasMatch(email)){
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Please enter valid email "),
      duration: Duration(seconds: 1),
      backgroundColor: Colors.red,
    ));
//      Fluttertoast.showToast(
//          msg: "Please enter valid email ",
//          toastLength: Toast.LENGTH_SHORT,
//          gravity: ToastGravity.CENTER,
//          timeInSecForIos: 1,
//          backgroundColor: Colors.red,
//          textColor: Colors.white,
//          fontSize: 15.0
//      );
    }
    else if(!regex1.hasMatch(password)) {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text("Password must contain one capital letter, two small letters, one digit and a special character and at least 8 characters in length"),
      duration: Duration(seconds: 1),
      backgroundColor: Colors.red,
    ));
//      Fluttertoast.showToast(
//          msg: "password must contain one capital letter a, two small letter ,one digit and special character ",
//          toastLength: Toast.LENGTH_SHORT,
//          gravity: ToastGravity.CENTER,
//          timeInSecForIos: 1,
//
//          backgroundColor: Colors.red,
//          textColor: Colors.white,
//          fontSize: 15.0
//      );
    }
  else if(map["success"]){
//    SharedPreferences prefs = await SharedPreferences.getInstance();
//    prefs.setString('email', "$email");

//    print(map["data"]["email"]);

    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs?.setBool("isLoggedIn", true);
    prefs?.setString("userEmail", email);

//    Future<void> main() async {
//      SharedPreferences prefs = await SharedPreferences.getInstance();
//      var status = prefs.getBool('isLoggedIn') ?? false;
//      print(status);
//      runApp(MaterialApp(home: status == true ? Signin() : Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Dashboard()))  ));
//    }

    print("case 1");
    Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Dashboard()));

  }  else {
    _scaffoldKey.currentState.showSnackBar(SnackBar(
      content: Text(map["message"]),
      duration: Duration(seconds: 1),
      backgroundColor: Colors.red,
    ));
//    Fluttertoast.showToast(
//        msg: map["message"],
//        toastLength: Toast.LENGTH_SHORT,
//        gravity: ToastGravity.CENTER,
//        timeInSecForIos: 1,
//        backgroundColor: Colors.red,
//        textColor: Colors.white,
//        fontSize: 15.0
//    );
  }

  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop:_onWillPop,
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
children: <Widget>[
  Stack(
          overflow: Overflow.visible,
          children: <Widget>[
            Container(
              height: MediaQuery.of(context).size.height * 0.28,
              width: double.infinity,
              child: WaveWidget(
                config: CustomConfig(
                  colors: [
                    Colors.white70,
                    Colors.white54,
                    Colors.white30,
                    Colors.white24,
                  ],
                  durations: [32000, 21000, 18000, 5000],
                  heightPercentages: [0.75, 0.56, 0.58, 0.31],
                ),
                backgroundColor: Color(0xFFb81f6f),
                size: Size(double.infinity,double.infinity),
              ),
            ),

            Positioned(
                  top: 130,
                  left: 50,
                  child: Text("Sign in",style: TextStyle(fontSize: 30,fontWeight: FontWeight.bold,color: Color(0xFF002632)),)),

          ],
  ),


  Center(
      child: Container(
        height: MediaQuery.of(context).size.height *0.70,
        width: MediaQuery.of(context).size.width * 0.79,
//          color: Colors.white,
//      margin: EdgeInsets.all(10.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: MediaQuery.of(context).size.height *0.10,),
//          SizedBox(height:20),
            Text(" Email",style: TextStyle(fontWeight: FontWeight.bold,color: Color(0xFF002632)),),
           SizedBox(height: 5,),
            Center(
              child: Container(
//                height: MediaQuery.of(context).size.height*0.0730,
//                width: MediaQuery.of(context).size.width *0.77,
                decoration: BoxDecoration(

                  borderRadius:BorderRadius.all((Radius.circular((5.0))),
                  ),
                  border: Border.all(
                      color:  Color(0xFFdbdbdb),width: 1,
                  ),
                ),
                child: TextFormField(
                  onFieldSubmitted: (String value) {
                    FocusScope.of(context).requestFocus(textSecondFocusNode);
                  },
                  controller: _text,
                  textInputAction: TextInputAction.next,
                  cursorColor: Colors.black,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: "Example@website.com",
                    hintStyle: TextStyle(fontSize: 13,color:Colors.black26),

                    prefixIcon:
                    Icon(CupertinoIcons.mail,color: Color(0xFF8a8989),size: 20,),
//                    focusedBorder: UnderlineInputBorder(
//                        borderSide: BorderSide(color: Color(0xFFdbdbdb),width: 2)
//                    ),
                  ),
                ),
              ),
            ),

            SizedBox(height: MediaQuery.of(context).size.height *0.02,),
            Text(" Password",style: TextStyle(fontWeight: FontWeight.bold,color: Color(0xFF002632)),),
            SizedBox(height: 5,),
            Center(
              child: Container(
//                height: MediaQuery.of(context).size.height*0.10,
//                width: MediaQuery.of(context).size.width *0.77,
                decoration: BoxDecoration(

                  borderRadius:BorderRadius.all((Radius.circular((5.0))),
                  ),
                  border: Border.all(
                    color:  Color(0xFFdbdbdb),width: 1,
                  ),
                ),
                child:TextFormField(
                  focusNode: textSecondFocusNode,
                  keyboardType: TextInputType.text,
                  controller: _text1,
                  obscureText: passwordVisible,//This will obscure text dynamically
                  decoration: InputDecoration(
                    border: InputBorder.none,
//                    labelText: 'Password',
                  prefixIcon:
                  Icon(CupertinoIcons.padlock,color:Color(0xFF8a8989),size: 20,),
                    hintText: 'Enter your password',
                    hintStyle: TextStyle(fontSize: 13,color:Colors.black26),
                    // Here is key idea
                    suffixIcon: IconButton(
                      icon: Icon(
                        // Based on passwordVisible state choose the icon
                        passwordVisible
                            ?Icons.visibility_off
                            : CupertinoIcons.eye_solid,
                        color: Color(0xFF8a8989),
                        size: 20,
                      ),
                      onPressed: () {
                        // Update the state i.e. toogle the state of passwordVisible variable
                        setState(() {
                          passwordVisible = !passwordVisible;
                        });
                      },
                    ),
                  ),
                ),


//                PasswordField(
//                  controller: _text1,
//                  color: Color(0xFFdbdbdb),
//                  hintText: "Atleast 8 digits",
//                  hintStyle: TextStyle(fontSize: 18,color: Colors.black38),
//                  pattern: "^(?=.*[A-Z])(?=.*[!@#*&])(?=.*[0-9])(?=.*[a-z].*[a-z]).{8}",
////                    pattern: r'.*[@$#.*].*',
////                autoFocus: true,
//                  errorFocusedBorder: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(4),
//                      borderSide: BorderSide(width: 1, color:  Color(0xFFdbdbdb))
//                  ),
//                  border: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(4),
//                      borderSide: BorderSide(width: 1, color: Color(0xFFdbdbdb))),
////                                focusedErrorBorder: OutlineInputBorder(
////                                borderSide: BorderSide(
////                                  color: Colors.grey[400],
////                                ),
//
//                  focusedBorder: OutlineInputBorder(
//                      borderRadius: BorderRadius.circular(4),
//                      borderSide: BorderSide(width: 1, color:  Color(0xFFdbdbdb))),
//                  errorMessage: 'must contain 1[!@#*&] + 1[0-9] + 1[A-Z] + 2[a-z]' ,
//                ),
              ),
            ),
            SizedBox(height: 5,),
           Row(
             mainAxisAlignment: MainAxisAlignment.end,
             children: <Widget>[
               GestureDetector(
                 onTap: (){
                   Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: ForgotPassword()));
                 },
                 child: Text("Forgot Password ?",style: TextStyle(fontSize: 15,
                   color: Colors.grey[400],
//                 Color(0xFF777777),
                   fontWeight: FontWeight.w600,),
                 ),
               ),
             ],
           ),
            SizedBox(height: MediaQuery.of(context).size.height *0.04,),

//            Center(
//              child: InkWell(
//                onTap: () {
//                  setState(() {
//                    if(_text.text.isEmpty){
//                      Fluttertoast.showToast(
//      msg: "Please enter email id",
//      toastLength: Toast.LENGTH_SHORT,
//      gravity: ToastGravity.CENTER,
//      timeInSecForIos: 1,
//      backgroundColor: Colors.grey[400],
//      textColor: Colors.white,
//      fontSize: 15.0
//    );
//                    }
//                    else if(_text1.text.length < 6){
//                      Fluttertoast.showToast(
//                          msg: "Please enter atleast 6 characters",
//                          toastLength: Toast.LENGTH_SHORT,
//                          gravity: ToastGravity.CENTER,
//                          timeInSecForIos: 1,
//                          backgroundColor: Colors.grey[400],
//                          textColor: Colors.white,
//                          fontSize: 15.0
//                      );
//                    }
//                   else if(_text1.text.isEmpty) {
//                      Fluttertoast.showToast(
//                          msg: "Please enter password",
//                          toastLength: Toast.LENGTH_SHORT,
//                          gravity: ToastGravity.CENTER,
//                          timeInSecForIos: 1,
//                          backgroundColor: Colors.grey[400],
//                          textColor: Colors.white,
//                          fontSize: 15.0
//                      );
//                    }  else
//                      {
//                      postDataAPI();
////                        Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Dashboard()));
//                      }
//
//                  });
//                },
//                child: Container(
//                  height: MediaQuery.of(context).size.height * 0.06,
//                  width: MediaQuery.of(context).size.width * 0.27,
//                  decoration: BoxDecoration(
//                    borderRadius:BorderRadius.all((Radius.circular((4.0))),
//                    ),
//
//                  ),
//                  child: Card(
//                    color:Colors.greenAccent,
//                    shape: RoundedRectangleBorder(
//                      side: BorderSide(color: Colors.greenAccent, width: 1),
//                      borderRadius: BorderRadius.circular(4.0),
//                    ),
//                    child: Center(child: Text("Sign in",style: TextStyle(color: Colors.white),),),
//                  ),
//                ),
//              ),
//            ),

            Visibility(
                visible: isApiCalling,
                child:  Center(
                  child: SizedBox(
                    height: 40,
                    width: 40,
                    child: CircularProgressIndicator(),
                  ),
                )

            ),
            Visibility(
                visible: !isApiCalling,
                child: Center(
                  child: MaterialButton(
                    shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                    minWidth: 35,
                    height: MediaQuery.of(context).size.height * 0.0533,
                    onPressed: (){

                      setState(() async{
                        if(_text.text.isEmpty) {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text("Please enter email"),
                            duration: Duration(seconds: 1),
                            backgroundColor: Colors.red,
                          ));
                        }
                        else if(_text1.text.isEmpty) {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text("Please enter password"),
                            duration: Duration(seconds: 1),
                            backgroundColor: Colors.red,
                          ));
                        }
                        else if(!regex.hasMatch(_text.value.text)){
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text("Please enter valid email"),
                            duration: Duration(seconds: 1),
                            backgroundColor: Colors.red,
                          ),);

                        }
                        else if(_text1.text.length < 8 && !regex1.hasMatch(_text1.value.text)){
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text("Password must contain one capital letter, two small letters, one digit and a special character and at least 8 characters in length"),
                            duration: Duration(seconds: 1),
                            backgroundColor: Colors.red,
                          ));
                        }
                        else
                        {
                          isApiCalling = true;
                          setState(() {

                          });
                          postDataAPI();
                        }
                      });
                    },
                    color: Color(0xFFb81f6f),
                    child:
                    Text("Sign in",style: TextStyle(color: Colors.white),) ,
                  ),
                ),
            ),


            SizedBox(height: MediaQuery.of(context).size.height *0.19,),
            GestureDetector(
                onTap: (){
                  setState(() {
//                      _text.text.isEmpty? _validate =true :
                    Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: LoginPage()));
                   });

//                  Navigator.push(
//                    context,
//                    MaterialPageRoute(builder: (context) => Dashboard()),
//                  );
                },
                child:       GestureDetector(
                    onTap: (){
                      Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: LoginPage()));
                    },
                    child:Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text("Don't have an account ?",style: TextStyle(fontSize: 15,color:Color(0xFF002632) ,fontWeight: FontWeight.w600,),),
                        Text(" Sign up",style: TextStyle(fontSize: 15,color:Color(0xFFb81f6f) ,fontWeight: FontWeight.w600,decoration: TextDecoration.underline,),),

//                                Center(child: Text("Already a member? Sign in",style: TextStyle(fontSize: 15,color:Color(0xFF002632) ,fontWeight: FontWeight.w600,decoration: TextDecoration.underline,),)))
                      ],
                    )
                ),

//                Center(child: Text("Don't have an account? Sign up",style: TextStyle(fontSize: 15,color: Colors.teal,fontWeight: FontWeight.w600,decoration: TextDecoration.underline,),
//                ),
//                ),
            ),
            SizedBox(height: MediaQuery.of(context).size.height *0.01,),





          ],
        ),

      ),
  ),

],


          ),
        ),
      ),
    );
  }
}
