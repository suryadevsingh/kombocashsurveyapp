import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:surveyapp/Screens/Personalinfo.dart';
import 'package:surveyapp/Screens/survey/pojo/MockSurveyPojo.dart';

import 'Dashboard.dart';
//                        arguments: mockSurveyPojo);
class Instruction extends StatefulWidget {
  MockSurveyPojo mockSurveyPojo;

  Instruction(MockSurveyPojo mockSurveyPojo) {
    this.mockSurveyPojo = mockSurveyPojo;
  }

  @override
  _InstructionState createState() => _InstructionState(mockSurveyPojo);
}

class _InstructionState extends State<Instruction> {
  MockSurveyPojo mockSurveyPojo;
  _InstructionState(MockSurveyPojo mockSurveyPojo){
    this.mockSurveyPojo = mockSurveyPojo;
  }


  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to go back to the survey?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () {
              Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Dashboard()));
            },
            child: new Text('Yes'),
          ),
        ],
      ),
    ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        body: Center(
          child: Container(
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.1344,
                  ),
                  Image.asset(
                    "assets/Images/launcher.png",
                    height: MediaQuery.of(context).size.height * 0.27,
                    width: 170,
                  ),
//                  SizedBox(height: MediaQuery.of(context).size.height *0.009,),
                  Text(
                    "Would you help me",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 22,
                        fontWeight: FontWeight.w500),
                  ),
//                  SizedBox(height: MediaQuery.of(context).size.height *0.02,),
                  Text(
                    "please ?",
                    style: TextStyle(
                        color: Colors.black,
                        fontSize: 22,
                        fontWeight: FontWeight.w500),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.025,
                  ),
                  Text(
                    "My task is to collect information",
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                        fontWeight: FontWeight.w300),
                  ),
//                  SizedBox(height: MediaQuery.of(context).size.height *0.0040,),
                  Text(
                    "about product and services.And",
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                        fontWeight: FontWeight.w300),
                  ),
//                  SizedBox(height: MediaQuery.of(context).size.height *0.01,),
                  Text(
                    "your feedback is valuable for us",
                    style: TextStyle(
                        color: Colors.grey,
                        fontSize: 15,
                        fontWeight: FontWeight.w300),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        "It will take less than 10 minutes ",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                            fontWeight: FontWeight.w300),
                      ),
                      Text(
                        "to complete this survey.If you ",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                            fontWeight: FontWeight.w300),
                      ),
                      Text(
                        "are a survey or please hand ",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                            fontWeight: FontWeight.w300),
                      ),
                      Text(
                        "over the device to the user .",
                        style: TextStyle(
                            color: Colors.grey,
                            fontSize: 15,
                            fontWeight: FontWeight.w300),
                      ),
                    ],
                  ),

                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.17,
                  ),
                  RaisedButton(
                    onPressed: () {
                      Navigator.of(context).push(PageTransition(
                          type: PageTransitionType.slideZoomLeft,
                          child: Personalinfo(mockSurveyPojo)));


                      print("Instruction mocksurveypojo");
                      print(mockSurveyPojo);


                    },
                    color: Color(0xFFb81f6f),
                    elevation: 2,
                    shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
//                 textColor: Color(),
                    child: Text(
                      "Let's get started",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
