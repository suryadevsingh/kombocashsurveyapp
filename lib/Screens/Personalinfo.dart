import 'package:better_uuid/uuid.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:flutter_page_transition/page_transition_type.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:surveyapp/Screens/survey/pojo/MockSurveyPojo.dart';
import 'package:surveyapp/backend/db_provider.dart';

import 'Dashboard.dart';

class Personalinfo extends StatefulWidget {
  MockSurveyPojo mockSurveyPojo;

  Personalinfo(MockSurveyPojo mockSurveyPojo) {
    this.mockSurveyPojo = mockSurveyPojo;
  }

  @override
  _PersonalinfoState createState() => _PersonalinfoState(mockSurveyPojo);
}

class _PersonalinfoState extends State<Personalinfo> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  MockSurveyPojo mockSurveyPojo;
  _PersonalinfoState(MockSurveyPojo mockSurveyPojo) {
    this.mockSurveyPojo = mockSurveyPojo;
  }

//  String validateEmail(String value) {
//    Pattern pattern1 ="^(?=.*[A-Z])(?=.*[%!@#*&])(?=.*[0-9])(?=.*[a-z].*[a-z]).{8}" ;
////    Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
//    Pattern pattern = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
//    RegExp regex = new RegExp(pattern);
//    String email =Useremailid.text;
////    RegExp regex1 = new RegExp(pattern1);
//    if(!regex.hasMatch(email)){
//      _scaffoldKey.currentState.showSnackBar(SnackBar(
//        content: Text("Please enter valid email "),
//        duration: Duration(seconds: 1),
//        backgroundColor: Colors.red,
//      ),);
//
//    }
//  }

  RegExp regex = new RegExp(
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$');

  var id = Uuid.v1();
  final Username = TextEditingController();
  final Useremailid = TextEditingController();
  final Contactnumber = TextEditingController();
  final Address = TextEditingController();
  final city = TextEditingController();
  String _selectedGender = null;

  FocusNode textSecondFocusNode = new FocusNode();
  FocusNode textthirdFocusNode = new FocusNode();
  FocusNode texfourthFocusNode = new FocusNode();
  FocusNode textfiveFocusNode = new FocusNode();
  FocusNode textsixFocusNode = new FocusNode();


  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to go back to the survey?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () {
              Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Dashboard()));
              },
            child: new Text('Yes'),
          ),
        ],
      ),
    )
        ??
        false;
  }



  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        key: _scaffoldKey,
        resizeToAvoidBottomPadding: false,
        resizeToAvoidBottomInset: true,
        body: SingleChildScrollView(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.07,
              ),
              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height * 0.04,
                  width: MediaQuery.of(context).size.width * 0.75,
                  child: Center(
                      child: Text(
                    "Fill your details",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500),
                  )),
                ),
              ),
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.05,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "Enter Name",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                    width: MediaQuery.of(context).size.width * 0.77,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        (Radius.circular((4.0))),
                      ),
                      border: Border.all(color: Colors.grey[400], width: 2),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (String value) {
                        FocusScope.of(context).requestFocus(textSecondFocusNode);
                      },
                      controller: Username,
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        hintStyle: TextStyle(fontSize: 13,color:Colors.black26),
                        hintText: "John doe",
                        icon: SizedBox(
                          width: 5,
                        ),
//                      prefixIcon:
//                          Icon(Icons.account_circle, color: Colors.grey[700]),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey[400])),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Text(
                    "Email Id",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                    width: MediaQuery.of(context).size.width * 0.77,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        (Radius.circular((4.0))),
                      ),
                      border: Border.all(color: Colors.grey[400], width: 2),
                    ),
                    child: TextFormField(
                      focusNode: textSecondFocusNode,
                      onFieldSubmitted: (String value) {
                        FocusScope.of(context).requestFocus(textthirdFocusNode);
                      },
                      controller: Useremailid,
//                    validator: validateEmail,
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.emailAddress,
                      textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        icon: SizedBox(
                          width: 5,
                        ),
                        hintText: "example@website.com",
                        hintStyle: TextStyle(fontSize: 13,color:Colors.black26),
//                      prefixIcon: Icon(Icons.mail, color: Colors.grey[700]),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey[400])),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Text(
                    "Contact number",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                    width: MediaQuery.of(context).size.width * 0.77,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        (Radius.circular((4.0))),
                      ),
                      border: Border.all(color: Colors.grey[400], width: 2),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (String value) {
                        FocusScope.of(context).requestFocus(texfourthFocusNode);
                      },
                      inputFormatters: [
                        LengthLimitingTextInputFormatter(15),
                      ],
                      focusNode: textthirdFocusNode,
                      controller: Contactnumber,
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.number,
                      textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                        icon: SizedBox(
                          width: 5,
                        ),
                        border: InputBorder.none,
                        hintStyle: TextStyle(fontSize: 13,color:Colors.black26),
                        hintText: "+66 1435 - 5674 -123",
//                      prefixIcon: Icon(Icons.call, color: Colors.grey[700]),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey[400])),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Text(
                    "Address",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                    width: MediaQuery.of(context).size.width * 0.77,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        (Radius.circular((4.0))),
                      ),
                      border: Border.all(color: Colors.grey[400], width: 2),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (String value) {
                        FocusScope.of(context).requestFocus(textfiveFocusNode);
                      },
                      focusNode: texfourthFocusNode,
                      controller: Address,
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                        border: InputBorder.none,
                        icon: SizedBox(
                          width: 5,
                        ),
                        hintText: "Block no. Floor no. City name",
                        hintStyle: TextStyle(fontSize: 13,color:Colors.black26),
//                      prefixIcon:
//                          Icon(Icons.location_on, color: Colors.grey[700]),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey[400])),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Text(
                    "City",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                    width: MediaQuery.of(context).size.width * 0.77,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        (Radius.circular((4.0))),
                      ),
                      border: Border.all(color: Colors.grey[400], width: 2),
                    ),
                    child: TextFormField(
                      onFieldSubmitted: (String value) {
                        FocusScope.of(context).requestFocus(textsixFocusNode);
                      },
                      focusNode: textfiveFocusNode,
                      controller: city,
                      cursorColor: Colors.black,
                      keyboardType: TextInputType.text,
                      textInputAction: TextInputAction.next,
                      decoration: InputDecoration(
                        hintText: "Ghaana",
                        icon: SizedBox(
                          width: 5,
                        ),
                        border: InputBorder.none,
                        hintStyle: TextStyle(fontSize: 13,color:Colors.black26),
//                      prefixIcon:
//                          Icon(Icons.my_location, color: Colors.grey[700]),
                        focusedBorder: UnderlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey[400])),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: MediaQuery.of(context).size.height * 0.02,
                  ),
                  Text(
                    "Gender",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  SizedBox(
                    height: 5,
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * 0.07,
                    width: MediaQuery.of(context).size.width * 0.77,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        (Radius.circular((4.0))),
                      ),
                      border: Border.all(
                        color: Colors.grey[400],
                        width: 2,
                      ),
                    ),
                    child: DropdownButton(
                      focusNode: textsixFocusNode,
                       isExpanded: true,
//                    icon:Icon(Icons.account_circle),
                      value: _selectedGender,
                      items: _dropDownItem(),
                      isDense: false,
                      elevation: 0,
//                    icon: Icon(Icons.account_circle),
                      onChanged: (value) {
                        _selectedGender = value;
                        setState(() {
                          _selectedGender = value;
                        });
                        print(_selectedGender);
                      },
                      hint: Text("      Select Gender",style:TextStyle(fontSize: 13,color:Colors.black26),),

                    ),
                  ),
                  SizedBox(
                    height: 60,
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
//              SizedBox(width: 40,),
                  OutlineButton(
                    shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                    borderSide: BorderSide(color: Color(0xFFb81f6f)),
                    child: Text(
                      "Back",
                      style: TextStyle(color: Color(0xFFb81f6f)),
                    ),
                  ),
//              SizedBox(width: 100,),
                  MaterialButton(
                    onPressed: () {
                      setState(() {
                        if (Username.text.isEmpty) {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text("Please enter username"),
                            duration: Duration(seconds: 1),
                            backgroundColor: Colors.red,
                          ));
//                        Fluttertoast.showToast(
//                            msg: "Please enter username",
//                            toastLength: Toast.LENGTH_SHORT,
//                            gravity: ToastGravity.CENTER,
//                            timeInSecForIos: 1,
//                            backgroundColor: Colors.red,
//                            textColor: Colors.white,
//                            fontSize: 15.0);
                        } else if (Useremailid.text.isEmpty) {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text("Please enter email"),
                            duration: Duration(seconds: 1),
                            backgroundColor: Colors.red,
                          ));
//                        Fluttertoast.showToast(
//                            msg: "Please enter email",
//                            toastLength: Toast.LENGTH_SHORT,
//                            gravity: ToastGravity.CENTER,
//                            timeInSecForIos: 1,
//                            backgroundColor: Colors.red,
//                            textColor: Colors.white,
//                            fontSize: 15.0);
                        }else if (!regex.hasMatch(Useremailid.text)) {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text("Wrong email address entered"),
                            duration: Duration(seconds: 1),
                            backgroundColor: Colors.red,
                          ));
                        } else if (Contactnumber.text.isEmpty) {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text("Please enter contact number"),
                            duration: Duration(seconds: 1),
                            backgroundColor: Colors.red,
                          ));
                        }else if (Contactnumber.text.length < 9) {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text("Please enter atleat 9 digits"),
                            duration: Duration(seconds: 1),
                            backgroundColor: Colors.red,
                          ));

                        } else if (Address.text.isEmpty) {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text("Please enter address"),
                            duration: Duration(seconds: 1),
                            backgroundColor: Colors.red,
                          ));

//                        Fluttertoast.showToast(
//                            msg: "Please enter address",
//                            toastLength: Toast.LENGTH_SHORT,
//                            gravity: ToastGravity.CENTER,
//                            timeInSecForIos: 1,
//                            backgroundColor: Colors.red,
//                            textColor: Colors.white,
//                            fontSize: 15.0);
                        } else if (city.text.isEmpty) {
                          _scaffoldKey.currentState.showSnackBar(SnackBar(
                            content: Text("Please enter address"),
                            duration: Duration(seconds: 1),
                            backgroundColor: Colors.red,
                          ));

                        }
                         else {
                          ////response storing in db

                          startSurvey();

//                        Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: MockSurvey()));

                        }
                      });

                      ///data sharing one page to another
//                    Navigator.of(context).push(PageTransition(
//                        type: PageTransitionType.slideZoomLeft,
//                        child: Question1(
//                          userid: id.time.toString(),
//                        )));
//
//                    <<<<<<< HEAD
//=======
//                    Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Question1(userid: id.time.toString(),)));
//>>>>>>> 67fdca8a8d54b78b04cc975f56ea4ecfe09b39db
                    },
                    shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                    color: Color(0xFFb81f6f),
                    child: Text(
                      "Next",
                      style: TextStyle(color: Colors.white),
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  void startSurvey() async{
    String userID = id.time.toString();
    Map<String, dynamic> row = {
      DBProvider.Username: Username.text,
      DBProvider.Useremailid: Useremailid.text,
      DBProvider.Contactnumber: Contactnumber.text,
      DBProvider.Address: Address.text,
      DBProvider.city: city.text,
      DBProvider.gender: _selectedGender.toString(),
      DBProvider.user_id: userID,
      DBProvider.survey_id: mockSurveyPojo.surveyId,
    };
    print(id.time);

    print("userID");
    print(userID);

    SharedPreferences perfs = await SharedPreferences.getInstance();
    perfs.setString("userId", userID);
    DBProvider.db.getpersonalinfo(row);

    print("personalinfo row");
    print(row);

    Navigator.pushNamed(context, "/mock_survey",
        arguments: mockSurveyPojo);
  }
}

List<DropdownMenuItem<String>> _dropDownItem() {
  List<String> ddl = ["Male", "Female", "Others"];
  return ddl
      .map((value) => DropdownMenuItem(
            value: value,
            child: Text("            $value"),
          ))
      .toList();
}
