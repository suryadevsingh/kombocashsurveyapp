
import 'dart:convert';

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:surveyapp/Screens/Question3.dart';
import 'package:surveyapp/backend/db_provider.dart';

class Question1 extends StatefulWidget {
  final String userid ;
  const Question1({Key key,@required this.userid}) : super(key: key);
  @override
  _Question1State createState() => _Question1State();
}

class _Question1State extends State<Question1> {
  String idresponse;
  List data ;
  String question_id;
  String choices;
  String option_id;
  String createdAt;


  int selectedRadioTile;
  setSelectedRadioTile(int val) {
    setState(() {
      selectedRadioTile = val;
    });
  }

  @override
  void initState() {
    super.initState();
    selectedRadioTile = null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.height *0.08,),
              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height *0.04,
                  width: MediaQuery.of(context).size.width *0.75,
                  child: FAProgressBar(
                    backgroundColor: Colors.grey[300],
                    progressColor: Colors.greenAccent[700],
                    currentValue: 40,
//                  displayText: "%",
                  ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height *0.06,),
              Container(
                  height: MediaQuery.of(context).size.height *0.20,
                  width: MediaQuery.of(context).size.height *0.40,

                decoration: BoxDecoration(
                  color: Color(0xFFf3f3f3),
                  borderRadius:BorderRadius.all((Radius.circular((4.0))),

                  ),
                  border: Border.all(
                      color: Colors.grey[400],width: 2
                  ),
                ),
             /// question
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.center,
//                children: <Widget>[
//                  SizedBox(height: 27,),
//                  Text("Are you satisfied with the", style: TextStyle(fontWeight: FontWeight.w600,fontSize: 22),),
//                  SizedBox(height: 5,),
//                  Text("Service of the product and", style: TextStyle(fontWeight: FontWeight.w600,fontSize: 22),),
//                  SizedBox(height: 5,),
//                  Text("services ?", style: TextStyle(fontWeight: FontWeight.w600,fontSize: 22),)
//                ],
//              ),
//            ),
              child: FutureBuilder(
                future: DBProvider.db.getallquestions(),
                builder: (BuildContext context, AsyncSnapshot snapshot) {
                  if (!snapshot.hasData) {
                    return Center(
                      child: CircularProgressIndicator(),
                    );
                  } else {
//          return Text(context.toString());
//        Text(DBProvider.survey_name);
                    return ListView.separated(
                      separatorBuilder: (context, index) => Divider(
                        color: Colors.black12,
                      ),
                      itemCount: snapshot.data.length,
//            Survey.fromJson(snapshot.data).toString().length,
//            snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Text(snapshot.data[index]["questionName"].toString(),);

                      },
                    );
                  }
                },
              ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height *0.04,),
            Container(
              width: MediaQuery.of(context).size.width *0.82,
              height: MediaQuery.of(context).size.height *0.34,
              child: FutureBuilder(
                future: DBProvider.db.getallchoices(),
                builder: (context,snapshot){
//                  var Survey = json.decode((snapshot.data.toString()));
                  return ListView.builder(
                    itemCount: snapshot.data.length,
                    itemBuilder: (BuildContext context ,int index){
                      return RadioListTile(
                        value: index,
                        title: AutoSizeText(
                          snapshot.data[index]["choices"].toString(),
                          style: TextStyle(fontSize: 18),
                          maxFontSize: 20,
                          minFontSize: 6,
                        ),
                        activeColor: Colors.blue,
                        groupValue:selectedRadioTile,
                        onChanged: (val){
                          print(val.toString());
                          print("radio tile pressed option_id ${snapshot.data[val]["option_id"]}");
                          print("radio tile pressed question_id ${snapshot.data[val]["question_id"]}");
                          print("radio tile pressed createdAt ${snapshot.data[val]["createdAt"]}");
                          print("radio tile pressed choices ${snapshot.data[val]["choices"]}");
                          print("radio tile pressed usesid ${widget.userid.toString()}");
                          idresponse = snapshot.data[val]["choices"];

                          setSelectedRadioTile(val);
                          Map<String, dynamic> row = {
                            DBProvider.question_id : snapshot.data[val]["question_id"].toString(),
                            DBProvider.idresponse : snapshot.data[val]["option_id"].toString(),
                            DBProvider.createdAt : snapshot.data[val]["createdAt"].toString(),
                            DBProvider.choices : snapshot.data[val]["choices"].toString(),
//                            DBProvider.option_id : snapshot.data[val]["option_id"].toString(),
//                            DBProvider.choices : snapshot.data[val]["choices"].toString(),
                            DBProvider.user_id : widget.userid.toString(),
                          };

//                          DBProvider.db.getresponse(row);
//                          return idresponse;
                        },
                      );
                    },
                  );
                },
              ),
            ),

SizedBox(height: MediaQuery.of(context).size.height *0.08,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  OutlineButton(onPressed: (){
                    Navigator.pop(context);
                  },
                    borderSide: BorderSide(color: Colors.greenAccent),
                    child: Text("Back",style: TextStyle(color: Colors.greenAccent),),),
                  MaterialButton(
                    onPressed: (){

                      Map<String, dynamic> row = {
//                DBProvider.survey_id: survey_id.toString(),
                        DBProvider.question_id:question_id.toString(),
                        DBProvider.choices: choices.toString(),
                        DBProvider.user_id: widget.userid.toString(),
                        DBProvider.idresponse : idresponse.toString(),
                        DBProvider.createdAt : createdAt.toString(),
                      };
                      DBProvider.db.getresponse(row);

                      Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Question3()));
                    },
                    color: Colors.greenAccent,
                    child: Text("Next",style: TextStyle(color: Colors.white),),
                  ),
                ],
              ),

            ],
          ),
        ),
      ),
    );
  }
}


