

import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';
import 'package:flutter_animation_progress_bar/flutter_animation_progress_bar.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:surveyapp/Screens/Surveycompleted.dart';
import 'package:surveyapp/backend/db_provider.dart';

class Question3 extends StatefulWidget {
  @override
  _Question3State createState() => _Question3State();
}

class _Question3State extends State<Question3> {

  bool _isChecked = true;
  bool _isChecked1 = true;
  bool _isChecked2 = true;
  bool _isChecked3 = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              SizedBox(height: MediaQuery.of(context).size.height *0.08,),
              Center(
                child: Container(
                  height: MediaQuery.of(context).size.height *0.04,
                  width: MediaQuery.of(context).size.width *0.75,
                  child: FAProgressBar(
                    backgroundColor: Colors.grey[300],
                    progressColor: Colors.greenAccent[700],
                    currentValue: 80,
//                  displayText: "%",
                  ),
                ),
              ),
              SizedBox(height: MediaQuery.of(context).size.height *0.06,),
              Container(
                height: MediaQuery.of(context).size.height *0.20,
                width: MediaQuery.of(context).size.height *0.40,

                decoration: BoxDecoration(
                  color: Color(0xFFf3f3f3),
                  borderRadius:BorderRadius.all((Radius.circular((4.0))),

                  ),
                  border: Border.all(
                      color: Colors.grey[400],width: 2
                  ),
                ),
                /// question
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
//                  SizedBox(height: 27,),
                    Text("Select the features you like", style: TextStyle(fontWeight: FontWeight.w600,fontSize: 22),),
                    SizedBox(height: 5,),
                    Text("the most about this", style: TextStyle(fontWeight: FontWeight.w600,fontSize: 22),),
                    SizedBox(height: 5,),
                    Text("application", style: TextStyle(fontWeight: FontWeight.w600,fontSize: 22),),
                  ],
                ),
//        child:    FutureBuilder(builder: (context, snapshot){
//              var showData =json.decode(snapshot.data.toString());
//              return ListView.builder(
//                itemBuilder: (BuildContext context , int index){
//                  return
//                    Text(showData[0].question_title);
////                    subtitle: Text(showData[index]["height"]),
//
//                },
//                itemCount: showData.length,
//              );
//            },
//              future: DefaultAssetBundle.of(context).loadString("load_json/survey.json"),
//            ),

              ),
              SizedBox(height: MediaQuery.of(context).size.height *0.06,),
//          Expanded(
//            child: FutureBuilder(builder: (context, snapshot){
//              var showData =json.decode(snapshot.data.toString());
//              return ListView.builder(
//                itemBuilder: (BuildContext context , int index){
//                  return RadioListTile(
//                  title: Text(showData[index]["option"]["options"]),
//                  activeColor: Colors.blue,
//                    groupValue: selectedRadioTile,
//                    onChanged: null,
//                    value: index,
//
//                  );
//                },
//                itemCount: showData.length,
//              );
//            },
//              future: DefaultAssetBundle.of(context).loadString("load_json/person.json"),
//            ),
//          ),
            Column(
              mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  height:  MediaQuery.of(context).size.height *0.27,
                  width:  MediaQuery.of(context).size.width *0.78,
                  child:
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.start,
//                    children: <Widget>[
//                      Checkbox(
//                        value: _isChecked,
//                        activeColor: Colors.blue,
//                        checkColor: Colors.white,
//                        onChanged: (bool val) => setState(() => _isChecked = val),
//                      ),
////                    Text("E - Commerce",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 15),),
//                      AutoSizeText(
//                        "E - Commerce",
//                        style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),
//                        maxFontSize: 15,
//                        minFontSize: 6,
//
//                      ),
////                    SizedBox(width: MediaQuery.of(context).size.width *0.40,),
//                    ],
//                  ),
                  FutureBuilder(
                    future: DBProvider.db.getallchoices(),
                    builder: (context ,snapshot){
                      return ListView.builder(
                      itemCount: snapshot.data.length,
                        itemBuilder: (BuildContext context ,int index){
                        return  Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[

                      Checkbox(
                        value: snapshot.data,
                        activeColor: Colors.blue,
                        checkColor: Colors.white,
                        onChanged: (bool val) {
                          setState(() {
                            val = snapshot.data[index]["option_id"];
                            print(val);
                          });
                        }
                      ),
                      AutoSizeText(
                        snapshot.data[index]["choices"].toString(),
                        style: TextStyle(fontSize: 15,fontWeight: FontWeight.bold),
                        maxFontSize: 15,
                        minFontSize: 6,

                      ),
//                    SizedBox(width: MediaQuery.of(context).size.width *0.40,),
                    ],
                  );
                        },
                      );
                    },
                  ),


                ),
              ],
            ),


              SizedBox(height: MediaQuery.of(context).size.height *0.08,),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
//                SizedBox(width: MediaQuery.of(context).size.width * 0.09,),
                  OutlineButton(onPressed: (){
                    Navigator.pop(context);
                  },
                    borderSide: BorderSide(color: Colors.greenAccent),
                    child: Text("Back",style: TextStyle(color: Colors.greenAccent),),),
//                SizedBox(width: MediaQuery.of(context).size.width* 0.40,),
                  MaterialButton(
                    onPressed: (){
//                      Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Surveycompleted()));
//                    Navigator.push(
//                      context,
//                      MaterialPageRoute(builder: (context) => Surveycompleted()),
//                    );
                    },
                    color: Colors.greenAccent,
                    child: Text("Next",style: TextStyle(color: Colors.white),),
                  ),
//                SizedBox(width: MediaQuery.of(context).size.width * 0.08,),
                ],
              ),

            ],
          ),
        ),
      ),
    );
  }
}


