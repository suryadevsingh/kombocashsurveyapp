import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_page_transition/flutter_page_transition.dart';
import 'package:surveyapp/Screens/Dashboard.dart';
import 'package:surveyapp/Screens/Instruction.dart';
import 'survey/pojo/MockQuestionPojo.dart';
import 'survey/pojo/MockSurveyPojo.dart';

class Surveycompleted extends StatefulWidget {
  MockSurveyPojo mockSurveyPojo;

  Surveycompleted(MockSurveyPojo  mockSurveyPojo) {
    this.mockSurveyPojo = mockSurveyPojo;
  }

  @override
  _SurveycompletedState createState() =>
      _SurveycompletedState(mockSurveyPojo);
}

class _SurveycompletedState extends State<Surveycompleted> {
  MockSurveyPojo mockSurveyPojo;

  _SurveycompletedState(MockSurveyPojo  mockSurveyPojo) {
    this.mockSurveyPojo = mockSurveyPojo;
  }

  Future<bool>alert(){
    if (mockSurveyPojo.totalQuestions == PageController(initialPage: 0))  {

      return _onWillPop();

    }
    else{
//    return _onWillPop() ;
    }
  }

  Future<bool> _onWillPop() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to go back to the survey?'),
        actions: <Widget>[
          new FlatButton(
            onPressed: () => Navigator.of(context).pop(false),
            child: new Text('No'),
          ),
          new FlatButton(
            onPressed: () {
              Navigator.of(context).push(PageTransition(type: PageTransitionType.slideZoomLeft, child: Dashboard()));
            },
            child: new Text('Yes'),
          ),
        ],
      ),
    ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: alert,
      child: Scaffold(
        body: SingleChildScrollView(
          child: Center(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.1344,
                ),
                Image.asset(
                  "assets/Images/shoppinglist.png",
                  width: 150,
                  fit: BoxFit.cover,
                ),
                SizedBox(
                  height: 15,
                ),
                Text(
                  "Thank You!",
                  style: TextStyle(
                      color: Colors.black,
                      fontSize: 30,
                      fontWeight: FontWeight.w500),
                ),
                SizedBox(
                  height: 30,
                ),
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      "Thank you for taking out time and",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                          fontWeight: FontWeight.w300),
                    ),
                    Text(
                      "filling this survey for us.This will",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                          fontWeight: FontWeight.w300),
                    ),
                    Text(
                      "help us to improve our services",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                          fontWeight: FontWeight.w300),
                    ),
//                    Text("",style: TextStyle(color: Colors.grey,fontSize:26,fontWeight: FontWeight.w300),),
                    SizedBox(
                      height: 16,
                    ),
                    Text(
                      "Wish you a great day ahead.",
                      style: TextStyle(
                          color: Colors.grey,
                          fontSize: 15,
                          fontWeight: FontWeight.w300),
                    ),
                  ],
                ),
                SizedBox(
                  height: MediaQuery.of(context).size.height * 0.050,
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
//                    SizedBox(width: 20,),
                    OutlineButton(
                      onPressed: () {

                            Navigator.of(context).push(PageTransition(
                            type: PageTransitionType.slideZoomLeft,
                            child: Instruction(mockSurveyPojo)));

                        },
                        shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                        borderSide: BorderSide(color: Color(0xFFb81f6f)),
                        child: Text(
                        "Re-take Survey",
                        style: TextStyle(color: Color(0xFFb81f6f)),
                      ),
                    ),
//                    SizedBox(width: 80,),
                    MaterialButton(
                      shape: RoundedRectangleBorder(borderRadius:BorderRadius.circular(5.0) ),
                      onPressed: () {
                        Navigator.of(context).push(PageTransition(
                            type: PageTransitionType.slideZoomLeft,
                            child: Dashboard()));
                      },
                      elevation: 1,
                      color: Color(0xFFb81f6f),
                      child: Text(
                        "Dashboard",
                        style: TextStyle(color: Colors.white),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
