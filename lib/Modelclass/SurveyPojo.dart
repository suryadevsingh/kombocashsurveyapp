//class SurveyPojo {
//  String message;
//  bool success;
//  List<Data> data;
//
//  SurveyPojo({this.message, this.success, this.data});
//
//  SurveyPojo.fromJson(Map<String, dynamic> json) {
//    message = json['message'];
//    success = json['success'];
//    if (json['data'] != null) {
//      data = new List<Data>();
//      json['data'].forEach((v) {
//        data.add(new Data.fromJson(v));
//      });
//    }
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['message'] = this.message;
//    data['success'] = this.success;
//    if (this.data != null) {
//      data['data'] = this.data.map((v) => v.toJson()).toList();
//    }
//    return data;
//  }
//}
//
//class Data {
//  List<QuestionId> questionId;
//  bool isActive;
//  bool newSurvey;
//  String sId;
//  String surveyId;
//  String surveyTitle;
//  String surveyDescription;
//  String startDate;
//  String endDate;
//  int iV;
//
//  Data(
//      {this.questionId,
//        this.isActive,
//        this.newSurvey,
//        this.sId,
//        this.surveyId,
//        this.surveyTitle,
//        this.surveyDescription,
//        this.startDate,
//        this.endDate,
//        this.iV});
//
//  Data.fromJson(Map<String, dynamic> json) {
//    if (json['questionId'] != null) {
//      questionId = new List<QuestionId>();
//      json['questionId'].forEach((v) {
//        questionId.add(new QuestionId.fromJson(v));
//      });
//    }
//    isActive = json['is_active'];
//    sId = json['_id'];
//    surveyId = json['survey_id'];
//    surveyTitle = json['survey_title'];
//    newSurvey = json['new_survey'];
//    surveyDescription = json['survey_description'];
//    startDate = json['startDate'];
//    endDate = json['endDate'];
//    iV = json['__v'];
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    if (this.questionId != null) {
//      data['questionId'] = this.questionId.map((v) => v.toJson()).toList();
//    }
//    data['is_active'] = this.isActive;
//    data['_id'] = this.sId;
//    data['survey_id'] = this.surveyId;
//    data['new_survey'] = this.newSurvey;
//    data['survey_title'] = this.surveyTitle;
//    data['survey_description'] = this.surveyDescription;
//    data['startDate'] = this.startDate;
//    data['endDate'] = this.endDate;
//    data['__v'] = this.iV;
//    return data;
//  }
//}
//
//class QuestionId {
//  bool isActive;
//  bool isSelected;
//  List<OptionId> optionId;
//  List<String> surveyId;
//  String sId;
//  String cloneQuestionId;
//  String cloneQuestionTitle;
//  String cloneQuestion;
//  String cloneQuestionType;
//  String createdAt;
//  String lastUpdate;
//  int iV;
//
//  QuestionId(
//      {this.isActive,
//        this.isSelected,
//        this.optionId,
//        this.surveyId,
//        this.sId,
//        this.cloneQuestionId,
//        this.cloneQuestionTitle,
//        this.cloneQuestion,
//        this.cloneQuestionType,
//        this.createdAt,
//        this.lastUpdate,
//        this.iV});
//
//  QuestionId.fromJson(Map<String, dynamic> json) {
//    isActive = json['is_active'];
//    isSelected = json['is_selected'];
//    if (json['optionId'] != null) {
//      optionId = new List<OptionId>();
//      json['optionId'].forEach((v) {
//        optionId.add(new OptionId.fromJson(v));
//      });
//    }
//    surveyId = json['surveyId'].cast<String>();
//    sId = json['_id'];
//    cloneQuestionId = json['cloneQuestion_id'];
//    cloneQuestionTitle = json['cloneQuestion_title'];
//    cloneQuestion = json['clone_question'];
//    cloneQuestionType = json['cloneQuestion_type'];
//    createdAt = json['createdAt'];
//    lastUpdate = json['lastUpdate'];
//    iV = json['__v'];
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['is_active'] = this.isActive;
//    data['is_selected'] = this.isSelected;
//    if (this.optionId != null) {
//      data['optionId'] = this.optionId.map((v) => v.toJson()).toList();
//    }
//    data['surveyId'] = this.surveyId;
//    data['_id'] = this.sId;
//    data['cloneQuestion_id'] = this.cloneQuestionId;
//    data['cloneQuestion_title'] = this.cloneQuestionTitle;
//    data['clone_question'] = this.cloneQuestion;
//    data['cloneQuestion_type'] = this.cloneQuestionType;
//    data['createdAt'] = this.createdAt;
//    data['lastUpdate'] = this.lastUpdate;
//    data['__v'] = this.iV;
//    return data;
//  }
//}
//
//class OptionId {
//  bool isActive;
//  List<String> cloneQuestionId;
//  String sId;
//  String cloneOptionId;
//  String cloneOption;
//  String cloneQuestionId1;
//  String createdAt;
//  String lastUpdated;
//  int iV;
//
//  OptionId(
//      {this.isActive,
//        this.cloneQuestionId,
//        this.sId,
//        this.cloneOptionId,
//        this.cloneOption,
//        this.cloneQuestionId1,
//        this.createdAt,
//        this.lastUpdated,
//        this.iV});
//
//  OptionId.fromJson(Map<String, dynamic> json) {
//    isActive = json['is_active'];
//    cloneQuestionId = json['cloneQuestionId'].cast<String>();
//    sId = json['_id'];
//    cloneOptionId = json['cloneOption_id'];
//    cloneOption = json['clone_option'];
//    cloneQuestionId1 = json['cloneQuestion_id'];
//    createdAt = json['createdAt'];
//    lastUpdated = json['lastUpdated'];
//    iV = json['__v'];
//  }
//
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['is_active'] = this.isActive;
//    data['cloneQuestionId'] = this.cloneQuestionId;
//    data['_id'] = this.sId;
//    data['cloneOption_id'] = this.cloneOptionId;
//    data['clone_option'] = this.cloneOption;
//    data['cloneQuestion_id'] = this.cloneQuestionId1;
//    data['createdAt'] = this.createdAt;
//    data['lastUpdated'] = this.lastUpdated;
//    data['__v'] = this.iV;
//    return data;
//  }
//}


class SurveyPojo {
  String message;
  bool success;
  List<Data> data;

  SurveyPojo({this.message, this.success, this.data});

  SurveyPojo.fromJson(Map<String, dynamic> json) {
    message = json['message'];
    success = json['success'];
    if (json['data'] != null) {
      data = new List<Data>();
      json['data'].forEach((v) {
        data.add(new Data.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['message'] = this.message;
    data['success'] = this.success;
    if (this.data != null) {
      data['data'] = this.data.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Data {
  List<QuestionId> questionId;
  bool isActive;
  bool newSurvey;
  String sId;
  String surveyId;
  String surveyTitle;
  String surveyDescription;
  String startDate;
  String endDate;
  int iV;

  Data(
      {this.questionId,
        this.isActive,
        this.newSurvey,
        this.sId,
        this.surveyId,
        this.surveyTitle,
        this.surveyDescription,
        this.startDate,
        this.endDate,
        this.iV});

  Data.fromJson(Map<String, dynamic> json) {
    if (json['questionId'] != null) {
      questionId = new List<QuestionId>();
      json['questionId'].forEach((v) {
        questionId.add(new QuestionId.fromJson(v));
      });
    }
    isActive = json['is_active'];
    newSurvey = json['new_survey'];
    sId = json['_id'];
    surveyId = json['survey_id'];
    surveyTitle = json['survey_title'];
    surveyDescription = json['survey_description'];
    startDate = json['startDate'];
    endDate = json['endDate'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.questionId != null) {
      data['questionId'] = this.questionId.map((v) => v.toJson()).toList();
    }
    data['is_active'] = this.isActive;
    data['new_survey'] = this.newSurvey;
    data['_id'] = this.sId;
    data['survey_id'] = this.surveyId;
    data['survey_title'] = this.surveyTitle;
    data['survey_description'] = this.surveyDescription;
    data['startDate'] = this.startDate;
    data['endDate'] = this.endDate;
    data['__v'] = this.iV;
    return data;
  }
}

class QuestionId {
  bool isActive;
  bool isSelected;
  List<OptionId> optionId;
  List<String> surveyId;
  String sId;
  String cloneQuestionId;
  String cloneQuestionId_;
  String cloneQuestion;
  String cloneQuestionType;
  String createdAt;
  String lastUpdate;
  int iV;

  QuestionId(
      {this.isActive,
        this.isSelected,
        this.optionId,
        this.surveyId,
        this.sId,
        this.cloneQuestionId_,
        this.cloneQuestionId,
        this.cloneQuestion,
        this.cloneQuestionType,
        this.createdAt,
        this.lastUpdate,
        this.iV});

  QuestionId.fromJson(Map<String, dynamic> json) {
    isActive = json['is_active'];
    isSelected = json['is_selected'];
    if (json['optionId'] != null) {
      optionId = new List<OptionId>();
      json['optionId'].forEach((v) {
        optionId.add(new OptionId.fromJson(v));
      });
    }
    surveyId = json['surveyId'].cast<String>();
    sId = json['_id'];
    cloneQuestionId = json['cloneQuestionId'];
    cloneQuestionId = json['cloneQuestion_id'];
    cloneQuestion = json['clone_question'];
    cloneQuestionType = json['cloneQuestion_type'];
    createdAt = json['createdAt'];
    lastUpdate = json['lastUpdate'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['is_active'] = this.isActive;
    data['is_selected'] = this.isSelected;
    if (this.optionId != null) {
      data['optionId'] = this.optionId.map((v) => v.toJson()).toList();
    }
    data['surveyId'] = this.surveyId;
    data['_id'] = this.sId;
    data['cloneQuestionId'] = this.cloneQuestionId;
    data['cloneQuestion_id'] = this.cloneQuestionId;
    data['clone_question'] = this.cloneQuestion;
    data['cloneQuestion_type'] = this.cloneQuestionType;
    data['createdAt'] = this.createdAt;
    data['lastUpdate'] = this.lastUpdate;
    data['__v'] = this.iV;
    return data;
  }
}

class OptionId {
  bool isActive;
  String sId;
  String cloneQuestionId;
  String cloneOptionId;
  String cloneOption;
  String cloneQuestionId_;
  String createdAt;
  String lastUpdated;
  int iV;

  OptionId(
      {this.isActive,
        this.sId,
        this.cloneQuestionId,
        this.cloneOptionId,
        this.cloneOption,
        this.cloneQuestionId_,
        this.createdAt,
        this.lastUpdated,
        this.iV});

  OptionId.fromJson(Map<String, dynamic> json) {
    isActive = json['is_active'];
    sId = json['_id'];
    cloneQuestionId = json['cloneQuestionId'];
    cloneOptionId = json['cloneOption_id'];
    cloneOption = json['clone_option'];
    cloneQuestionId_ = json['cloneQuestion_id'];
    createdAt = json['createdAt'];
    lastUpdated = json['lastUpdated'];
    iV = json['__v'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['is_active'] = this.isActive;
    data['_id'] = this.sId;
    data['cloneQuestionId'] = this.cloneQuestionId;
    data['cloneOption_id'] = this.cloneOptionId;
    data['clone_option'] = this.cloneOption;
    data['cloneQuestion_id'] = this.cloneQuestionId;
    data['createdAt'] = this.createdAt;
    data['lastUpdated'] = this.lastUpdated;
    data['__v'] = this.iV;
    return data;
  }
}
