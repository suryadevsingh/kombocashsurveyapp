// To parse this JSON data, do
//
//     final signUp = signUpFromJson(jsonString);

import 'dart:convert';

SignUp signUpFromJson(String str) => SignUp.fromJson(json.decode(str));

String signUpToJson(SignUp data) => json.encode(data.toJson());

class SignUp {
  String userName;
  String email;
  String password;

  SignUp({
    this.userName,
    this.email,
    this.password,
  });

  factory SignUp.fromJson(Map<String, dynamic> json) => SignUp(
    userName: json["userName"],
    email: json["email"],
    password: json["password"],
  );

  Map<String, dynamic> toJson() => {
    "userName": userName,
    "email": email,
    "password": password,
  };
}
