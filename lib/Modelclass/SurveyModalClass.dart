// To parse this JSON data, do
//
//     final survey = surveyFromJson(jsonString);

import 'dart:convert';

Survey surveyFromJson(String str) => Survey.fromJson(json.decode(str));

String surveyToJson(Survey data) => json.encode(data.toJson());

class Survey {
  String message;
  bool success;
  List<Datum> data;

  Survey({
    this.message,
    this.success,
    this.data,
  });

  factory Survey.fromJson(Map<String, dynamic> json) => Survey(
    message: json["message"],
    success: json["success"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "success": success,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  List<QuestionId> questionId;
  bool isActive;
  String id;
  String surveyId;
  String surveyTitle;
  String surveyDescription;
  DateTime startDate;
  DateTime endDate;
  int v;

  Datum({
    this.questionId,
    this.isActive,
    this.id,
    this.surveyId,
    this.surveyTitle,
    this.surveyDescription,
    this.startDate,
    this.endDate,
    this.v,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    questionId: List<QuestionId>.from(json["questionId"].map((x) => QuestionId.fromJson(x))),
    isActive: json["is_active"],
    id: json["_id"],
    surveyId: json["survey_id"],
    surveyTitle: json["survey_title"],
    surveyDescription: json["survey_description"],
    startDate: DateTime.parse(json["startDate"]),
    endDate: DateTime.parse(json["endDate"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "questionId": List<dynamic>.from(questionId.map((x) => x.toJson())),
    "is_active": isActive,
    "_id": id,
    "survey_id": surveyId,
    "survey_title": surveyTitle,
    "survey_description": surveyDescription,
    "startDate": startDate.toIso8601String(),
    "endDate": endDate.toIso8601String(),
    "__v": v,
  };
}

class QuestionId {
  bool isActive;
  bool isSelected;
  List<OptionId> optionId;
  List<String> surveyId;
  String id;
  String cloneQuestionId;
  String cloneQuestionTitle;
  String cloneQuestion;
  String cloneQuestionType;
  DateTime createdAt;
  DateTime lastUpdate;
  int v;

  QuestionId({
    this.isActive,
    this.isSelected,
    this.optionId,
    this.surveyId,
    this.id,
    this.cloneQuestionId,
    this.cloneQuestionTitle,
    this.cloneQuestion,
    this.cloneQuestionType,
    this.createdAt,
    this.lastUpdate,
    this.v,
  });

  factory QuestionId.fromJson(Map<String, dynamic> json) => QuestionId(
    isActive: json["is_active"],
    isSelected: json["is_selected"],
    optionId: List<OptionId>.from(json["optionId"].map((x) => OptionId.fromJson(x))),
    surveyId: List<String>.from(json["surveyId"].map((x) => x)),
    id: json["_id"],
    cloneQuestionId: json["cloneQuestion_id"],
    cloneQuestionTitle: json["cloneQuestion_title"],
    cloneQuestion: json["clone_question"],
    cloneQuestionType: json["cloneQuestion_type"],
    createdAt: DateTime.parse(json["createdAt"]),
    lastUpdate: DateTime.parse(json["lastUpdate"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "is_active": isActive,
    "is_selected": isSelected,
    "optionId": List<dynamic>.from(optionId.map((x) => x.toJson())),
    "surveyId": List<dynamic>.from(surveyId.map((x) => x)),
    "_id": id,
    "cloneQuestion_id": cloneQuestionId,
    "cloneQuestion_title": cloneQuestionTitle,
    "clone_question": cloneQuestion,
    "cloneQuestion_type": cloneQuestionType,
    "createdAt": createdAt.toIso8601String(),
    "lastUpdate": lastUpdate.toIso8601String(),
    "__v": v,
  };
}

class OptionId {
  bool isActive;
  List<String> cloneQuestionId;
  String id;
  String cloneOptionId;
  String cloneOption;
  String optionIdCloneQuestionId;
  DateTime createdAt;
  DateTime lastUpdated;
  int v;

  OptionId({
    this.isActive,
    this.cloneQuestionId,
    this.id,
    this.cloneOptionId,
    this.cloneOption,
    this.optionIdCloneQuestionId,
    this.createdAt,
    this.lastUpdated,
    this.v,
  });

  factory OptionId.fromJson(Map<String, dynamic> json) => OptionId(
    isActive: json["is_active"],
    cloneQuestionId: List<String>.from(json["cloneQuestionId"].map((x) => x)),
    id: json["_id"],
    cloneOptionId: json["cloneOption_id"],
    cloneOption: json["clone_option"],
    optionIdCloneQuestionId: json["cloneQuestion_id"],
    createdAt: DateTime.parse(json["createdAt"]),
    lastUpdated: DateTime.parse(json["lastUpdated"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "is_active": isActive,
    "cloneQuestionId": List<dynamic>.from(cloneQuestionId.map((x) => x)),
    "_id": id,
    "cloneOption_id": cloneOptionId,
    "clone_option": cloneOption,
    "cloneQuestion_id": optionIdCloneQuestionId,
    "createdAt": createdAt.toIso8601String(),
    "lastUpdated": lastUpdated.toIso8601String(),
    "__v": v,
  };
}
