
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:snack/snack.dart';



class Utility {
  static String PRODUCTION_URL = "http://kombocash.com/";
  static String UPLOAD_SURVEY = "addUserResponse";

  static void showToastMessages(String toastMessage) {
//    final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();



//    _scaffoldKey1.currentState.showSnackBar(SnackBar(
//      content: Text(toastMessage),
//      duration: Duration(seconds: 1),
//
//      backgroundColor: Colors.red,
//    ));
    
    Fluttertoast.showToast(
        msg: toastMessage,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
//        timeInSecForIos: 0,
        backgroundColor: Colors.red,
        textColor: Colors.white,
        fontSize: 15.0);
  }



  static Future<String> getCurrentUserIdGivingSurvey() async {
    SharedPreferences perfs = await SharedPreferences.getInstance();
    return perfs.getString("userId");
  }

  static Future<String> getLoggedInUserEmail() async {
    SharedPreferences perfs = await SharedPreferences.getInstance();
    return perfs.getString("userEmail");
  }
}

