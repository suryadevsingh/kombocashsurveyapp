
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';
import 'SurveyModalClass.dart';


class DBProvider {
  static Database _database;
  MethodChannel _channel =  MethodChannel('plugins.flutter.io/path_provider');
  static final DBProvider db = DBProvider._();

  DBProvider._();
//  static final Survey = 'Survey';
  static final Surveyquestion = 'Surveyquestion';
  static final Surveyresponse = 'Surveyresponse';

  static final new_survey = 'new_survey';

  static final Surveydata = 'Surveydata';
  static final Surveyoptions = 'Surveyoptions';
  static final survey_id ="survey_id";
  static final survey_name ="survey_name";
  static final survey_description ="survey_description";
  static final question_id ="question_id";
  static final questionName ="questionName";
  static final question_type ="question_type";
  static final option_id ="option_id";

  static final idresponse ="idresponse";
  static final Personalinfo ="Personalinfo";
  static final Username ="Username";
  static final Useremailid ="Useremailid";
  static final Contactnumber ="Contactnumber";
  static final city ="city";
  static final gender ="gender";
  static final Address ="Address";
  static final user_id ="user_id";
  static final message ="message";
  static final data ="data";
  static final status ="status";
  static final options ="options";
  static final choices ="choices";




  static final surveyId ="survey_id";
  static final surveyTitle ="survey_title";
  static final surveyDescription ="survey_description";
  static final startDate ="startDate";
  static final endDate ="endDate";
  static final totalQuestions ="total_questions";
  static final v ="v";
  static final questionId ="questionId";
  static final isActive ="isActive";
  static final id ="id";
  static final isSelected ="isSelected";
  static final cloneQuestionId ="cloneQuestion_id";
  static final cloneQuestionTitle ="cloneQuestion_title";
  static final cloneQuestion ="clone_question";
  static final cloneQuestionType ="cloneQuestion_type";
  static final createdAt ="createdAt";
  static final lastUpdate ="lastUpdate";




  Future<Database> get database async {
    // If database exists, return database
    if (_database != null) return _database;

    // If database don't exists, create one
    _database = await initDB();
    return _database;
  }
  Future<Directory> getApplicationDocumentsDirectory() async {
    final String path =
    await _channel.invokeMethod<String>('getApplicationDocumentsDirectory');
    if (path == null) {
      return null;
    }
    return Directory(path);
  }

  initDB() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentsDirectory.path, 'Survey.db');
    return await openDatabase(path, version: 22, onOpen: (db) {},
        onCreate: (Database db, int version)  async{
        await db.execute(
              '''CREATE TABLE $survey_name(
                  $surveyId VARCHAR ,
                  $surveyTitle VARCHAR,
                  $surveyDescription VARCHAR,
                  $startDate VARCHAR,
                  $endDate VARCHAR,
                  $totalQuestions VARCHAR,
                  $new_survey VARCHAR,
                  $question_type VARCHAR,
                  $v INT
              )'''
          );
       await db.execute(
              '''CREATE TABLE $Surveyquestion(
                  $surveyId VARCHAR,
                  $questionId VARCHAR,
                  $questionName VARCHAR,
                  $question_type VARCHAR
                  )'''
          );
        await db.execute('''
            CREATE TABLE $Surveyoptions(
                $choices VARCHAR,
                $option_id VARCHAR ,
                $question_id VARCHAR,
                $createdAt VARCHAR
                
                )'''
        );
        await db.execute('''
            CREATE TABLE $Surveyresponse(
                $question_id VARCHAR,
                $user_id VARCHAR,
                $option_id VARCHAR,
                $survey_id VARCHAR,
                $createdAt VARCHAR
                )'''
        );

        await db.execute('''
            CREATE TABLE $Personalinfo(
                $Username VARCHAR,
                $Useremailid VARCHAR,
                $Contactnumber VARCHAR,
                $Address VARCHAR,
                $city VARCHAR,
                $gender VARCHAR,
                $user_id VARCHAR,
                $survey_id VARCHAR
                )'''
        );

        });
  }



  Future<int> insert(Map<String, dynamic> row) async {
    Database db = await database;
    return await db.insert(survey_name, row);
  }

  Future<int> insertQuestion(Map<String, dynamic> row) async {
    Database db = await database;
    return await db.insert(Surveyquestion, row);
  }

  Future<int> insertChoices(Map<String, dynamic> row) async {
    Database db = await database;
    return await db.insert(Surveyoptions, row);
  }


  Future<int> deleteAllSurvey() async {
    final db = await database;
    var res = await db.rawDelete('DELETE FROM $survey_name');
    res = await db.rawDelete('DELETE FROM $Surveyquestion');
    res = await db.rawDelete('DELETE FROM $Surveyoptions');
    res = await db.rawDelete('DELETE FROM $Surveyresponse');
    res = await db.rawDelete('DELETE FROM $Personalinfo');
    return res;
  }

  Future<int> deleteOnlyAllSurvey() async {
    final db = await database;
    var res = await db.rawDelete('DELETE FROM $survey_name');
    res = await db.rawDelete('DELETE FROM $Surveyquestion');
    res = await db.rawDelete('DELETE FROM $Surveyoptions');
    return res;
  }


  Future<int> deleteAllperviousSurveyresponses() async {

    final db = await database;
    var res = await db.rawDelete('DELETE FROM $Personalinfo');
    return res;

  }

  Future<int> deleteAllPreviousOptionsStored() async {
    final db = await database;
    var res = await db.rawDelete('DELETE FROM $Surveyresponse');
    return res;
  }

//  Future<List<Map<String, dynamic>>> getallquestions() async {
//    Database db = await database;
//    return await db.query('$Surveyquestion');
//  }
  Future<List<Map<String, dynamic>>> getallquestions() async {
    Database db = await database;
    return await db.query('$Surveyquestion');
  }
  Future<List<Map<String, dynamic>>> getallchoices() async {
    Database db = await database;
    return await db.query('$Surveyoptions');
  }

  Future<List<Map<String, dynamic>>> getallSurevey() async {
    Database db = await database;
    return await db.query("$survey_name");
  }

  Future<List<Map<String, dynamic>>> fetchAllSurveyResponse() async {
    Database db = await database;
    return await db.query('$Surveyresponse');
  }

  Future<List<Map<String, dynamic>>> getAllSurvey() async {
    Database db = await database;
    return await db.query('$survey_name');
  }

  Future<List<Map<String, dynamic>>> getAllPersonalinfo() async {
    Database db = await database;
    return await db.query('$Personalinfo');
  }

  Future<List<Map<String, dynamic>>> fetchAllNewlyAddedSurvey() async {
    Database db = await database;
    return await db.query('$survey_name', where: '$new_survey = "true"');
  }

  Future<List<Map<String, dynamic>>> getthisSurveyoptions() async {
    Database db = await database;
    return await db.query(Surveyoptions,where: '$question_id = "101"');
  }

  Future<List<Map<String, dynamic>>> fetchAllUsersWithThisSurvey(String surveyId) async {
    Database db = await database;
    return await db.query(Personalinfo,where: '$survey_id = "$surveyId"');
  }

  Future<List<Map<String, dynamic>>> testfetchAllUsersWithThisSurvey(String userId,String surveyId) async {
    Database db = await database;
    return await db.rawQuery('SELECT * FROM "$Personalinfo" WHERE "$survey_id"=? AND "$user_id"=?', [surveyId,userId]);
  }


  Future<List<Map<String, dynamic>>> fetchAllSurveyResponseWhereUserIdAndSurveyId(String userId, String surveyId) async {
    Database db = await database;
    return await db.rawQuery('SELECT * FROM "$Surveyresponse" WHERE "$survey_id"=? AND "$user_id"=?', [surveyId,userId]);
  }


  Future<List<Map<String, dynamic>>> testfetchAllSurveyResponseWhereUserIdAndSurveyId(String userId) async {
    Database db = await database;
    return await db.rawQuery('SELECT * FROM "$Surveyresponse" WHERE "$user_id"=?', [userId]);
  }


  Future<List<Map<String, dynamic>>> fetchQuestionOptions(String qId) async {
    Database db = await database;
    print("Fetch Where Question Id  is " + qId);
    return await db.query(Surveyoptions,where: '$question_id = "$qId"');
  }

  Future<List<Map<String, dynamic>>> getthisSurveyquestion() async {
    Database db = await database;
    return await db.query(Surveyquestion,where: '$survey_id = "200"');
  }

  Future<List<Map<String, dynamic>>> fetchSurveyQuestionsBasedOnId(String surveyId) async {
    Database db = await database;
    return await db.query(Surveyquestion,where: '$survey_id = "$surveyId"');
  }

  Future<List<Map<String, dynamic>>> fetchQuestionResponse(String questionId, String userId) async {
    Database db = await database;

    //    db.query(Surveyresponse,where: '$survey_id = "$surveyId"');

    return await db.rawQuery('SELECT * FROM "$Surveyresponse" WHERE "$question_id"=? AND "$user_id"=?', [questionId,userId]);

  }

  Future<int> getresponse(Map<String, dynamic> row) async {
    Database db = await database;
    return await db.insert(Surveyresponse, row);
  }

  Future<int> addQuestionResponse(Map<String, dynamic> row) async {
    Database db = await database;
    return await db.insert(Surveyresponse, row);
  }

  Future<int> removeQuestionResponse(String qId, String userId, String optionId) async {
    Database db = await database;
    return await db.rawDelete('DELETE FROM "$Surveyresponse" WHERE "$question_id"=? AND "$user_id"=? AND "$option_id"=?',[qId,userId, optionId]);
  }

  Future<int> updateQuestionResponse(Map<String, dynamic> row) async {
    Database db = await database;
//    return await db.insert(Surveyresponse, row);
    return await db.update(Surveyresponse, row, where: '$question_id = ?', whereArgs: [row[question_id]]);
  }

  Future<List<Map<String, dynamic>>> findIfOptionisCheckedEarlier(String questionId, String userId, String optionId) async {
    Database db = await database;
//    return await db.insert(Surveyresponse, row);
    return await db.rawQuery('SELECT * FROM "$Surveyresponse" WHERE "$question_id"=? AND "$user_id"=? AND "$option_id"=?', [questionId,userId,optionId]);
  }

  Future<int> getpersonalinfo(Map<String, dynamic> row) async {
    Database db = await database;
    return await db.insert(Personalinfo, row);
  }

  Future<List<Map<String, dynamic>>> fetchAllUsers() async {
    Database db = await database;
    return await db.query(Personalinfo);
  }


//  Future<List<Map<String, dynamic>>> getcount() async {
//    Database db = await database;
//    return await db.query();
//  }

  Future<int> queryRowCount(Map<int, dynamic> row) async {
    int count ;
    Database db = await database;
    count = Sqflite.firstIntValue(await db.rawQuery('SELECT COUNT($surveyTitle) FROM $survey_name'));
  return count  ;

  }

}