// To parse this JSON data, do
//
//     final survey = surveyFromJson(jsonString);

import 'dart:convert';

Survey surveyFromJson(String str) => Survey.fromJson(json.decode(str));

String surveyToJson(Survey data) => json.encode(data.toJson());

class Survey {
  String message;
  bool success;
  List<Datum> data;

  Survey({
    this.message,
    this.success,
    this.data,
  });

  factory Survey.fromJson(Map<String, dynamic> json) => Survey(
    message: json["message"],
    success: json["success"],
    data: List<Datum>.from(json["data"].map((x) => Datum.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "message": message,
    "success": success,
    "data": List<dynamic>.from(data.map((x) => x.toJson())),
  };
}

class Datum {
  List<QuestionId> questionId;
  bool isActive;
  bool newSurvey;
  String id;
  String surveyId;
  String surveyTitle;
  String surveyDescription;
  DateTime startDate;
  DateTime endDate;
  int v;

  Datum({
    this.questionId,
    this.isActive,
    this.newSurvey,
    this.id,
    this.surveyId,
    this.surveyTitle,
    this.surveyDescription,
    this.startDate,
    this.endDate,
    this.v,
  });

  factory Datum.fromJson(Map<String, dynamic> json) => Datum(
    questionId: List<QuestionId>.from(json["questionId"].map((x) => QuestionId.fromJson(x))),
    isActive: json["is_active"],
    newSurvey: json["new_survey"],
    id: json["_id"],
    surveyId: json["survey_id"],
    surveyTitle: json["survey_title"],
    surveyDescription: json["survey_description"],
    startDate: DateTime.parse(json["startDate"]),
    endDate: DateTime.parse(json["endDate"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "questionId": List<dynamic>.from(questionId.map((x) => x.toJson())),
    "is_active": isActive,
    "new_survey": newSurvey,
    "_id": id,
    "survey_id": surveyId,
    "survey_title": surveyTitle,
    "survey_description": surveyDescription,
    "startDate": startDate.toIso8601String(),
    "endDate": endDate.toIso8601String(),
    "__v": v,
  };
}

class QuestionId {
  bool isActive;
  bool isSelected;
  List<OptionId> optionId;
  List<SurveyId> surveyId;
  String id;
  String cloneQuestionId;
  String questionIdCloneQuestionId;
  String cloneQuestion;
  CloneQuestionType cloneQuestionType;
  DateTime createdAt;
  DateTime lastUpdate;
  int v;
  String cloneQuestionTitle;

  QuestionId({
    this.isActive,
    this.isSelected,
    this.optionId,
    this.surveyId,
    this.id,
    this.cloneQuestionId,
    this.questionIdCloneQuestionId,
    this.cloneQuestion,
    this.cloneQuestionType,
    this.createdAt,
    this.lastUpdate,
    this.v,
    this.cloneQuestionTitle,
  });

  factory QuestionId.fromJson(Map<String, dynamic> json) => QuestionId(
    isActive: json["is_active"],
    isSelected: json["is_selected"],
    optionId: List<OptionId>.from(json["optionId"].map((x) => OptionId.fromJson(x))),
    surveyId: List<SurveyId>.from(json["surveyId"].map((x) => surveyIdValues.map[x])),
    id: json["_id"],
    cloneQuestionId: json["cloneQuestionId"] == null ? null : json["cloneQuestionId"],
    questionIdCloneQuestionId: json["cloneQuestion_id"],
    cloneQuestion: json["clone_question"],
    cloneQuestionType: cloneQuestionTypeValues.map[json["cloneQuestion_type"]],
    createdAt: DateTime.parse(json["createdAt"]),
    lastUpdate: DateTime.parse(json["lastUpdate"]),
    v: json["__v"],
    cloneQuestionTitle: json["cloneQuestion_title"] == null ? null : json["cloneQuestion_title"],
  );

  Map<String, dynamic> toJson() => {
    "is_active": isActive,
    "is_selected": isSelected,
    "optionId": List<dynamic>.from(optionId.map((x) => x.toJson())),
    "surveyId": List<dynamic>.from(surveyId.map((x) => surveyIdValues.reverse[x])),
    "_id": id,
    "cloneQuestionId": cloneQuestionId == null ? null : cloneQuestionId,
    "cloneQuestion_id": questionIdCloneQuestionId,
    "clone_question": cloneQuestion,
    "cloneQuestion_type": cloneQuestionTypeValues.reverse[cloneQuestionType],
    "createdAt": createdAt.toIso8601String(),
    "lastUpdate": lastUpdate.toIso8601String(),
    "__v": v,
    "cloneQuestion_title": cloneQuestionTitle == null ? null : cloneQuestionTitle,
  };
}

enum CloneQuestionType { SELECT_ONE, SELECT_MULTIPLE }

final cloneQuestionTypeValues = EnumValues({
  "Select Multiple": CloneQuestionType.SELECT_MULTIPLE,
  "Select One": CloneQuestionType.SELECT_ONE
});

class OptionId {
  bool isActive;
  List<String> cloneQuestionId;
  String id;
  String cloneOptionId;
  String cloneOption;
  String optionIdCloneQuestionId;
  DateTime createdAt;
  DateTime lastUpdated;
  int v;

  OptionId({
    this.isActive,
    this.cloneQuestionId,
    this.id,
    this.cloneOptionId,
    this.cloneOption,
    this.optionIdCloneQuestionId,
    this.createdAt,
    this.lastUpdated,
    this.v,
  });

  factory OptionId.fromJson(Map<String, dynamic> json) => OptionId(
    isActive: json["is_active"],
    cloneQuestionId: List<String>.from(json["cloneQuestionId"].map((x) => x)),
    id: json["_id"],
    cloneOptionId: json["cloneOption_id"],
    cloneOption: json["clone_option"],
    optionIdCloneQuestionId: json["cloneQuestion_id"],
    createdAt: DateTime.parse(json["createdAt"]),
    lastUpdated: DateTime.parse(json["lastUpdated"]),
    v: json["__v"],
  );

  Map<String, dynamic> toJson() => {
    "is_active": isActive,
    "cloneQuestionId": List<dynamic>.from(cloneQuestionId.map((x) => x)),
    "_id": id,
    "cloneOption_id": cloneOptionId,
    "clone_option": cloneOption,
    "cloneQuestion_id": optionIdCloneQuestionId,
    "createdAt": createdAt.toIso8601String(),
    "lastUpdated": lastUpdated.toIso8601String(),
    "__v": v,
  };
}

enum SurveyId { THE_5_E7_D73_B1_D3_D63_F0017_AD0835, THE_5_E7_C81_B395127_E0017_DD782_E, THE_5_E7_C3575201_A840017_D2_DF39, THE_5_E7213_B0_EA788600173_A3350 }

final surveyIdValues = EnumValues({
  "5e7213b0ea788600173a3350": SurveyId.THE_5_E7213_B0_EA788600173_A3350,
  "5e7c3575201a840017d2df39": SurveyId.THE_5_E7_C3575201_A840017_D2_DF39,
  "5e7c81b395127e0017dd782e": SurveyId.THE_5_E7_C81_B395127_E0017_DD782_E,
  "5e7d73b1d3d63f0017ad0835": SurveyId.THE_5_E7_D73_B1_D3_D63_F0017_AD0835
});

class EnumValues<T> {
  Map<String, T> map;
  Map<T, String> reverseMap;

  EnumValues(this.map);

  Map<T, String> get reverse {
    if (reverseMap == null) {
      reverseMap = map.map((k, v) => new MapEntry(v, k));
    }
    return reverseMap;
  }
}
