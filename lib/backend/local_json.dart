import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:surveyapp/Modelclass/SurveyPojo.dart';

import 'db_provider.dart';

class SurveyLocalJson {
  var Mydata;

  Future<List<String>> loadAsset(BuildContext context) async {
//    Mydata =DefaultAssetBundle.of(context).loadString('load_json/survey.json').then((data){
    var checkIfSurveyAvaialbe = await DBProvider.db.getAllSurvey();
    if (checkIfSurveyAvaialbe.length != 0) {
      List<String> a = new List();
      return Future(() => a);
    } else {
      Mydata = "http://kombocash.com/getSurvey";
      Response response = await get(Mydata);
      print(response.body);
      SurveyPojo obj = SurveyPojo.fromJson(json.decode(response.body));
      print("Survey Object");
      print(Mydata);
      for (var i = 0; i < obj.data.length; i++) {
        print(obj.data[i].surveyDescription);
        print("Survey surveyDescription");
        print("-----------");
        for (var j = 0; j < obj.data[i].questionId.length; j++) {
          print("Quesiton");
//          print(obj.data[i].questionId[j].cloneQuestionTitle);
//          print(obj.data[i].surveyQuestions[i].options[j].choices);
          print("-------");
          for (var k = 0; k < obj.data[i].questionId[j].optionId.length; k++) {
            print("choices");
            print(obj.data[i].questionId[j].optionId[k].sId);
            print("-------");
            Map<String, dynamic> row = {
              DBProvider.choices:
                  obj.data[i].questionId[j].optionId[k].cloneOption,
              DBProvider.createdAt:
                  obj.data[i].questionId[j].optionId[k].createdAt,
              DBProvider.option_id: obj.data[i].questionId[j].optionId[k].sId,
              DBProvider.question_id:
                  obj.data[i].questionId[j].sId,
            };
            DBProvider.db.insertChoices(row);
          }
          String quesType = "";
          if (obj.data[i].questionId[j].cloneQuestionType != "") {
            quesType = obj.data[i].questionId[j].cloneQuestionType;
          }
          Map<String, dynamic> row = {
            DBProvider.surveyId: obj.data[i].sId,
            DBProvider.questionId: obj.data[i].questionId[j].sId,
            DBProvider.questionName: obj.data[i].questionId[j].cloneQuestion,
            DBProvider.question_type: quesType,
          };
          print("Adding in Question");
          print(row);
          DBProvider.db.insertQuestion(row);
        }
        String questionType = "";
        if (obj.data[i].questionId.length > 0) {
          questionType = obj.data[i].questionId[0].cloneQuestionType;
        }
        Map<String, dynamic> row = {
          DBProvider.surveyId: obj.data[i].sId,
          DBProvider.surveyTitle: obj.data[i].surveyTitle,
          DBProvider.surveyDescription: obj.data[i].surveyDescription,
          DBProvider.startDate: obj.data[i].startDate,
          DBProvider.endDate: obj.data[i].endDate,
          DBProvider.totalQuestions: obj.data[i].questionId.length,
          DBProvider.new_survey: obj.data[i].newSurvey.toString(),
          DBProvider.question_type: questionType,
          DBProvider.v: obj.data[i].iV,
        };
        print("Survey");
        print(obj.data[i].sId);
        DBProvider.db.insert(row);
      }
    }
  }
}
