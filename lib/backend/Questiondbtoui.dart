


import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/material.dart';

import 'db_provider.dart';

class Questiondbtoui extends StatefulWidget {
  final String userid ;
  const Questiondbtoui({Key key,@required this.userid}) : super(key: key);
  @override
  _QuestiondbtouiState createState() => _QuestiondbtouiState();
}

class _QuestiondbtouiState extends State<Questiondbtoui> {
  String idresponse;
  String question_id;
//  String survey_id;
  String choices;
//  var id =Uuid.v1();
  var isLoading = false;
  int selectedRadioTile;
  setSelectedRadioTile(int val) {
    setState(() {
      selectedRadioTile = val;
    });
  }


  @override
  void initState() {
    super.initState();
    selectedRadioTile = 0;
  }
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body:
        Column(
          children: <Widget>[
            FutureBuilder(
              future: DBProvider.db.getthisSurveyquestion(),
              builder: (BuildContext context, AsyncSnapshot snapshot) {
                  return Expanded(
                    child: ListView.builder(
                      itemCount: snapshot.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Center(child: Text(snapshot.data[index]["question_type"].toString()));

                      },
                    ),
                  );

              },
            ),


      FutureBuilder(
        future: DBProvider.db.getallchoices(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Expanded(
              child: ListView.builder(
                itemCount: snapshot.data.length,
                itemBuilder: (BuildContext context, int index) {
                  return RadioListTile(
                    value: index,
                    title: AutoSizeText(
                      snapshot.data[index]["choices"].toString(),

                      style: TextStyle(fontSize: 18),
                      maxFontSize: 20,
                      minFontSize: 6,
                    ),
                    activeColor: Colors.blue,
                    groupValue: selectedRadioTile,
                    onChanged: (val)  {
                      print(val.toString());
                      print("radio tile pressed option_id ${snapshot.data[val]["option_id"]}");
                         idresponse = snapshot.data[val]["choices"];
//                      question_id = snapshot.data[val]["question_id"].toString();
//                      choices = snapshot.data[val]["choices"].toString();
//                      print(question_id);
//                      print(idresponse);
//                      print(choices);
//                      survey_id = snapshot.data["survey_id"].toString();

//                      print(survey_id);

                      setSelectedRadioTile(val);
                      Map<String, dynamic> row = {
//                        DBProvider.survey_id:snapshot.data["survey_id"].text,
                        DBProvider.question_id : snapshot.data[val]["question_id"].toString(),
                        DBProvider.idresponse : snapshot.data[val]["option_id"].toString(),
                        DBProvider.choices : snapshot.data[val]["choices"].toString(),
                      };

                      DBProvider.db.getresponse(row);
                      return idresponse;
                      },
                  );
                },
              ),
            );

        },
      ),



            SizedBox(height: 20,),

          MaterialButton(
            child: Text("summit"),
            onPressed: (){
//              print(idresponse);
//              print(widget.userid);
//              print(question_id);
//              print(idresponse);
//              print(choices);
              Map<String, dynamic> row = {
//                DBProvider.survey_id: survey_id.toString(),
                DBProvider.question_id:question_id.toString(),
                DBProvider.choices: choices.toString(),
                DBProvider.user_id: widget.userid.toString(),
                DBProvider.idresponse : idresponse.toString(),
              };
              DBProvider.db.getresponse(row);
//              print(id.time);
            },
          ),

          ],
        ),
      ),
    );
  }

}

